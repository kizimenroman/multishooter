// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MultiHUD.generated.h"

class UTexture2D;
class UCharacterOverlay;
class UUserWidget;
class UAnnouncementWidget;

USTRUCT(BlueprintType)
struct FHUDPackage
{	
	GENERATED_BODY()

public:

	UTexture2D* CrosshairsCentr;
	UTexture2D* CrosshairsLeft;
	UTexture2D* CrosshairsRight;
	UTexture2D* CrosshairsTop;
	UTexture2D* CrosshairsBottom;

	float CrosshairSpread;
	FLinearColor CrosshairsColor;
};

UCLASS()
class MULTISHOOTER_API AMultiHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, Category = "Setup")
	TSubclassOf<UUserWidget> CharacterOverlayClass;

	UPROPERTY(EditAnywhere, Category = "Setup")
	TSubclassOf<UUserWidget> AnnouncementWidgetClass;

	UPROPERTY()
	UCharacterOverlay* CharacterOverlay;

	UPROPERTY()
	UAnnouncementWidget* AnnounceWidget;

	virtual void DrawHUD() override;
	void AddCharacterOverlay();
	void AddAnouncement();

protected:
	virtual void BeginPlay() override;	

private:

	FHUDPackage HUDPackage;

	UPROPERTY(EditAnywhere)
	float CrosshairSpreadMax = 16.f;

	void DrawCrosshair(UTexture2D* Texture, FVector2D ViewportCentr, FVector2D Spread, FLinearColor CrosshairColor);

public:

	FORCEINLINE void SetHUDPackage(const FHUDPackage& Package) { HUDPackage = Package; }
};
