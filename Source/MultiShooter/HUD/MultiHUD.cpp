	// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiHUD.h"
#include "CharacterOverlay.h"
#include "AnnouncementWidget.h"
#include "GameFramework/PlayerController.h"

void AMultiHUD::BeginPlay()
{
	Super::BeginPlay();
}

void AMultiHUD::AddAnouncement()
{
	APlayerController* PController = GetOwningPlayerController();
	if (PController && AnnouncementWidgetClass)
	{
		AnnounceWidget = CreateWidget<UAnnouncementWidget>(PController, AnnouncementWidgetClass);
		AnnounceWidget->AddToViewport();
	}
}

void AMultiHUD::AddCharacterOverlay()
{
	APlayerController* PController = GetOwningPlayerController();
	if (PController && CharacterOverlayClass)
	{
		CharacterOverlay = CreateWidget<UCharacterOverlay>(PController, CharacterOverlayClass);
		CharacterOverlay->AddToViewport();
	}
}

void AMultiHUD::DrawHUD()
{
	Super::DrawHUD();

	FVector2D ViewportSize;
	if (GEngine)
	{
		GEngine->GameViewport->GetViewportSize(ViewportSize);
		const FVector2D ViewportCentr(ViewportSize.X * 0.5f, ViewportSize.Y * 0.5f);

		float SpreadScaled = CrosshairSpreadMax * HUDPackage.CrosshairSpread;

		if (HUDPackage.CrosshairsCentr)
		{
			FVector2D Spread(0.f,0.f);
			DrawCrosshair(HUDPackage.CrosshairsCentr, ViewportCentr, Spread, HUDPackage.CrosshairsColor);
		}
		if (HUDPackage.CrosshairsLeft)
		{
			FVector2D LeftSpread(-SpreadScaled, 0.f);
			DrawCrosshair(HUDPackage.CrosshairsLeft, ViewportCentr, LeftSpread, HUDPackage.CrosshairsColor);
		}
		if (HUDPackage.CrosshairsRight)
		{
			FVector2D RightSpread(SpreadScaled, 0.f);
			DrawCrosshair(HUDPackage.CrosshairsRight, ViewportCentr, RightSpread, HUDPackage.CrosshairsColor);
		}
		if (HUDPackage.CrosshairsTop)
		{
			FVector2D RightSpread(0.f, -SpreadScaled);
			DrawCrosshair(HUDPackage.CrosshairsTop, ViewportCentr, RightSpread, HUDPackage.CrosshairsColor);
		}
		if (HUDPackage.CrosshairsBottom)
		{
			FVector2D BottomSpread(0.f, SpreadScaled);
			DrawCrosshair(HUDPackage.CrosshairsBottom, ViewportCentr, BottomSpread, HUDPackage.CrosshairsColor);
		}
	}
}

void AMultiHUD::DrawCrosshair(UTexture2D* Texture, FVector2D ViewportCentr, FVector2D Spread, FLinearColor CrosshairColor)
{
	FVector2D TextureDrawPoint(ViewportCentr.X - (Texture->GetSizeX() * 0.5f) + Spread.X,
							   ViewportCentr.Y - (Texture->GetSizeY() * 0.5f) + Spread.Y
							   );

	DrawTexture(
		Texture, 
		TextureDrawPoint.X, 
		TextureDrawPoint.Y,
		Texture->GetSizeX(),
		Texture->GetSizeY(),
		0.f,
		0.f,
		1.f,
		1.f,
		CrosshairColor
		);
}