// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponTypes.h"
#include "BaseWeapon.generated.h"

UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	EWS_Initial UMETA(DisplayName = "Initial State"),
	EWS_Equipped UMETA(DisplayName = "Equipped"),
	EWS_EquippedBack UMETA(DisplayName = "Equipped Back"),
	EWS_Dropped UMETA(DisplayName = "Dropped"),

	EWS_MAX UMETA(DisplayName = "Default MAX")
};

UENUM(BlueprintType)
enum class EFireType : uint8
{
	EFT_HitScanWeapon UMETA(DesplayName = "HitScan Weapon"),
	EFT_ProjectileWeapon UMETA(DesplayName = "Projectile Weapon"),
	EFT_Shotgun UMETA(DesplayName = "Shotgun Weapon"),

	EFT_MAX UMETA(DisplayName = "Default Max")
};

class UTexture2D;
class ACasing;
class UAnimationAsset;
class UCapsuleComponent;
class UWidgetComponent;
class AMultiCharacter;
class AMultiPlayerController;
class USoundCue;


UCLASS()
class MULTISHOOTER_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	ABaseWeapon();
	virtual void Tick(float DeltaTime) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void OnRep_Owner() override;
	void SetHUDAmmo();
	void ShowPickupWidget(bool bShowWidget);
	virtual void Fire(const FVector& HitTarget);
	void Dropped();
	void AddAmmo(int32 AmmoToAdd);
	FVector TraceEndWithScatter(const FVector& HitTarget);
	//////////////////////////////////////
	/*
	* Crosshairs set
	*/
	UPROPERTY(EditAnywhere, Category = "Crosshairs")
	UTexture2D* CrossHairCentr;

	UPROPERTY(EditAnywhere, Category = "Crosshairs")
	UTexture2D*	CrosshairLeft; 

	UPROPERTY(EditAnywhere, Category = "Crosshairs")
	UTexture2D* CrosshairRight;

	UPROPERTY(EditAnywhere, Category = "Crosshairs")
	UTexture2D* CrosshairTop;

	UPROPERTY(EditAnywhere, Category = "Crosshairs")
	UTexture2D* CrosshairBottom;
	//////////////////////////////////////
	/*
	* Zoom FOV while aiming
	*/
	UPROPERTY(EditAnywhere)
	float ZoomedFOV = 30.f;

	UPROPERTY(EditAnywhere)
	float ZoomInterpSpeed = 20.f;
	//////////////////////////////////////
	/*
	* Fire
	*/
	UPROPERTY(EditAnywhere, Category = "Combat")
	float SpreadFire = 0.8f;

	UPROPERTY(EditAnywhere, Category = "Combat")
	float FireDelay = 0.15f;

	UPROPERTY(EditAnywhere, Category = "Combat")
	bool bAutomatic = true;
	//////////////////////////////////////
	/*
	* Sounds
	*/
	UPROPERTY(EditAnywhere, Category = "Combat")
	USoundCue* EquipSFX;

	/*
	* Enable or disable custom depth
	*/
	void EnableCustomDepth(bool bEnable);

	bool bDestroyedWeapon = false;

	UPROPERTY(EditAnywhere)
	EFireType FireType;

	UPROPERTY(EditAnywhere, Category = "Weapon Scatter")
	bool bUseScatter = false;


protected:

	virtual void BeginPlay() override;
	virtual void OnWeaponStateSet();
	virtual void OnDropped();
	virtual void OnEquipped();
	virtual void OnEquippedBack();

	UFUNCTION()
	virtual void OnCapsuleBeginOverlapped( 
		UPrimitiveComponent* OverlappedComponent, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, 
		bool bFromSweep, 
		const FHitResult& SweepResult
		);

	UFUNCTION()
	virtual void OnCapsuleEndOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	///////////////////////////
	/*
	* Trace satter
	*/
	UPROPERTY(EditAnywhere, Category = "Weapon Scatter")
	float DistanceToSphere = 800.f;

	UPROPERTY(EditAnywhere, Category = "Weapon Scatter")
	float SpreadRadius = 75.f;

private:
	////////////////////////////////////////////////////////
	/*
	* Setups
	*/
	UPROPERTY()
	AMultiCharacter* MultiOwnerCharacter;

	UPROPERTY()
	AMultiPlayerController* MultiOwnerController;

	UPROPERTY(EditAnywhere)
	EWeaponType WeaponType;

	UPROPERTY(VisibleAnywhere, Category = "Weapon properties")
	USkeletalMeshComponent* WeapMesh;

	UPROPERTY(VisibleAnywhere, Category = "Weapon properties")
	UCapsuleComponent* CapsulePickup;

	UPROPERTY(ReplicatedUsing = OnRep_WeaponState, VisibleAnywhere, Category = "Weapon properties")
	EWeaponState WeaponState;

	UPROPERTY(EditAnywhere, Category = "Weapon properties")
	UWidgetComponent* PickupWidget;

	UPROPERTY(EditAnywhere, Category = "Weapon Properties")
	UAnimationAsset* FireAnimation;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ACasing> CasingClass;

	UFUNCTION()
	void OnRep_WeaponState(); 
	////////////////////////////////////////////////////////
	/*
	* Ammo system
	*/
	UPROPERTY(EditAnywhere)
	int32 Ammo;

	UFUNCTION(Client, Reliable)
	void ClientUpdateAmmo(int32 ServerAmmo);

	UFUNCTION(Client, Reliable)
	void ClientAddAmmo(int32 AmmoToAdd);

	void SpendRound();

	UPROPERTY(EditAnywhere)
	int32 MagCapacity;

	// The number of processed server requests for Ammo.
	// Incremented in SpendRound, decremented in ClientUpdateAmmo.
	int32 AmmoSequence = 0;

public:	
	FORCEINLINE UCapsuleComponent* GetCapsule() const { return CapsulePickup; }
	FORCEINLINE USkeletalMeshComponent* GetWeaponMesh() const { return WeapMesh; }
	FORCEINLINE EWeaponType GetWeaponType() const { return WeaponType; }
	FORCEINLINE int32 GetAmmo() const { return Ammo; }
	FORCEINLINE int32 GetMagCapacity() const { return MagCapacity; }
	FORCEINLINE float GetZoomedFOV() const { return ZoomedFOV; }
	FORCEINLINE float GetZoomInterpSpeed() const { return ZoomInterpSpeed; }

	void SetWeapState(EWeaponState State);
	bool IsEmpty();
	bool IsFull();
};
