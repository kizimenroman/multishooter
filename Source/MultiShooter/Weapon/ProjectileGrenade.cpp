// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileGrenade.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"

AProjectileGrenade::AProjectileGrenade()
{
	ProjectileBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body Mesh"));
	ProjectileBody->SetupAttachment(RootComponent);
	ProjectileBody->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	GrenadeHandle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Greande handle"));
	GrenadeHandle->SetupAttachment(RootComponent);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->SetIsReplicated(true);
	ProjectileMovement->bShouldBounce = true;
}

void AProjectileGrenade::BeginPlay()
{
	AActor::BeginPlay();

	ProjectileMovement->OnProjectileBounce.AddDynamic(this, &AProjectileGrenade::OnBounce);
	SpawnTrailSystem();	
	StartDestroyTimer();
}

void AProjectileGrenade::OnBounce(const FHitResult& ImpactResult, const FVector& ImpactVelocity)
{
	if (BounceSfx)
	{
		UGameplayStatics::PlaySoundAtLocation(
			this,
			BounceSfx,
			GetActorLocation()
		);
	}
}

void AProjectileGrenade::Destroyed()
{
	ExplodeDamage();
	Super::Destroyed();
}