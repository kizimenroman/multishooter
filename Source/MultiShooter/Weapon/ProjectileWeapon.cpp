// Fill out your copyright notice in the Description page of Project Settings.


#include "Engine/SkeletalMeshSocket.h"
#include "Projectile.h"
#include "ProjectileWeapon.h"

void AProjectileWeapon::Fire(const FVector& HitTarget)
{
	Super::Fire(HitTarget);

	if(!HasAuthority()) return;

	APawn* InstagatorPawn = Cast<APawn>(GetOwner());
	const USkeletalMeshSocket* MuzzleSocket = GetWeaponMesh()->GetSocketByName(FName("MuzzleFlash"));
	if (MuzzleSocket)
	{
		FTransform SocketTransform = MuzzleSocket->GetSocketTransform(GetWeaponMesh());
		FVector ToTarget = HitTarget - SocketTransform.GetLocation();
		FRotator Direction = ToTarget.Rotation();

		if (ProjectileClass && InstagatorPawn)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = GetOwner();
			SpawnParams.Instigator = InstagatorPawn;

			if (GetWorld())
			{
				GetWorld()->SpawnActor<AProjectile>(
					ProjectileClass, 
					SocketTransform.GetLocation(),
					Direction,
					SpawnParams
					);
			}
		}
	}
}