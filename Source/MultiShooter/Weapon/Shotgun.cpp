// Fill out your copyright notice in the Description page of Project Settings.


#include "Shotgun.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "MultiShooter/Character/MultiCharacter.h"
#include "Kismet/KismetMathLibrary.h"

void AShotgun::FireShotgun(const TArray<FVector_NetQuantize>& HitTargets)
{
	ABaseWeapon::Fire(FVector());

	APawn* PawnOwner = Cast<APawn>(GetOwner());
	if (!PawnOwner) return;

		AController* ControllerInstigator = PawnOwner->GetController();

		const USkeletalMeshSocket* MuzzleFlashSocket = GetWeaponMesh()->GetSocketByName(FName("MuzzleFlash"));
		if (MuzzleFlashSocket)
		{
			const FTransform MuzzleSocketTransform = MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
			const FVector Start = MuzzleSocketTransform.GetLocation();

			// store all characters who's take spread damege
			TMap<AMultiCharacter*, uint32> ShotMap;
			for (FVector_NetQuantize HitTarget : HitTargets)
			{
				FHitResult Hit;
				WeaponTraceHit(Start, HitTarget, Hit);

				AMultiCharacter* HitCharacter = Cast<AMultiCharacter>(Hit.GetActor());
				if (HitCharacter)
				{
					if (ShotMap.Contains(HitCharacter))
					{
						ShotMap[HitCharacter]++;
					}
					else
					{
						ShotMap.Emplace(HitCharacter, 1);
					}

					if (ImpactFx)
					{
						UGameplayStatics::SpawnEmitterAtLocation(
							GetWorld(),
							ImpactFx,
							Hit.ImpactPoint,
							Hit.ImpactNormal.Rotation()
							);
					}

					if (HitSound)
					{
						UGameplayStatics::PlaySoundAtLocation(
							this,
							HitSound,
							Hit.ImpactPoint,
							.75f,
							FMath::FRandRange(-.5f, .5f)
						);
					}
				}
			}

			// Make damage such count upper
			for (auto HitPair : ShotMap)
			{
				if (HitPair.Key && HasAuthority() && ControllerInstigator)
				{
					UGameplayStatics::ApplyDamage(
						HitPair.Key, // Character that was hit
						DamageAmount * HitPair.Value, // Multiplay damage by number of times hit
						ControllerInstigator,
						this,
						UDamageType::StaticClass()
					);
				}
			}
		}
}

void AShotgun::ShotgunTraceWithScatter(const FVector& HitTarget, TArray<FVector_NetQuantize>& HitTargets)
{
	const USkeletalMeshSocket* MuzzleFlashSocket = GetWeaponMesh()->GetSocketByName(FName("MuzzleFlash"));
	if (!MuzzleFlashSocket) return;

		const FTransform MuzzleSocketTransform = MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
		const FVector TraceStart = MuzzleSocketTransform.GetLocation();

		const FVector ToTargetNormalized = (HitTarget - TraceStart).GetSafeNormal();
		const FVector SphereCenter = TraceStart + ToTargetNormalized * DistanceToSphere;		

		for (uint32 i = 0; i < CountPallets; i++)
		{
			const FVector RandVec = UKismetMathLibrary::RandomUnitVector() * FMath::FRandRange(0.f, SpreadRadius);
			const FVector EndLoc = SphereCenter + RandVec;
			FVector ToEndLoc = EndLoc - TraceStart;
			ToEndLoc = TraceStart + ToEndLoc * TRACE_LENGTH / ToEndLoc.Size();

			HitTargets.Add(ToEndLoc);
		}
}