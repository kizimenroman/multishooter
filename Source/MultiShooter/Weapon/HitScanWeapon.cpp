// Fill out your copyright notice in the Description page of Project Settings.


#include "HitScanWeapon.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "MultiShooter/Character/MultiCharacter.h"
#include "WeaponTypes.h"
#include "DrawDebugHelpers.h"

void AHitScanWeapon::Fire(const FVector& HitTarget)
{
	Super::Fire(HitTarget);

	APawn* PawnOwner = Cast<APawn>(GetOwner());
	if (!PawnOwner) return;

		AController* ControllerInstigator = PawnOwner->GetController();

		const USkeletalMeshSocket* MuzzleFlashSocket = GetWeaponMesh()->GetSocketByName(FName("MuzzleFlash"));
		if (MuzzleFlashSocket)
		{
			FTransform MuzzleSocketTransform = MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
			FVector Start = MuzzleSocketTransform.GetLocation();

			FHitResult FireHit;
			WeaponTraceHit( Start, HitTarget, FireHit);

			AMultiCharacter* HitCharacter = Cast<AMultiCharacter>(FireHit.GetActor());
			if (HitCharacter && HasAuthority() && ControllerInstigator)
			{
				UGameplayStatics::ApplyDamage(
					HitCharacter,
					DamageAmount,
					ControllerInstigator,
					this,
					UDamageType::StaticClass()
				);
			}

			if (MuzzleFx)
			{
				UGameplayStatics::SpawnEmitterAtLocation(
					GetWorld(),
					MuzzleFx,
					MuzzleSocketTransform
				);
			}

			if (MuzzleSound)
			{
				UGameplayStatics::PlaySoundAtLocation(
					this,
					MuzzleSound,
					Start
				);
			}

			if (ImpactFx)
			{
				UGameplayStatics::SpawnEmitterAtLocation(
					GetWorld(),
					ImpactFx,
					FireHit.ImpactPoint,
					FireHit.ImpactNormal.Rotation()
				);
					UE_LOG(LogTemp,Warning,TEXT("ImpactFxWasSpawned"));
			}

			if (HitSound)
			{
				UGameplayStatics::PlaySoundAtLocation(
					this,
					HitSound,
					FireHit.ImpactPoint
				);
			}
		}
}

void AHitScanWeapon::WeaponTraceHit(const FVector& TraceStart, const FVector& HitTarget, FHitResult& OutHit)
{	
	if (GetWorld())
	{
		FVector End = TraceStart + (HitTarget - TraceStart) * 1.25;

		GetWorld()->LineTraceSingleByChannel(
				OutHit,
				TraceStart,
				End,
				ECollisionChannel::ECC_Visibility
			);

		FVector BeamEnd = End;
		if (OutHit.bBlockingHit)
		{
			BeamEnd = OutHit.ImpactPoint;
		}

		if (BeamFx)
		{
			UParticleSystemComponent* BeamComponent = UGameplayStatics::SpawnEmitterAtLocation(
				GetWorld(),
				BeamFx,
				TraceStart,
				FRotator::ZeroRotator,
				true
			);

			if (BeamComponent)
			{
				BeamComponent->SetVectorParameter(FName("Target"), BeamEnd);
			}
		}
	}
}