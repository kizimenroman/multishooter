// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "ProjectileRocket.generated.h"

//class UNiagaraSystem;
//class UNiagaraComponent;
class USoundCue;
class UAudioComponent;
class USoundAttenuation;
class URocketMovementComponent;

UCLASS()
class MULTISHOOTER_API AProjectileRocket : public AProjectile
{
	GENERATED_BODY()

public:
	AProjectileRocket();
	virtual void Destroyed() override;

protected:
	virtual void BeginPlay() override;
	virtual void OnHitCallBack(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	UPROPERTY(EditAnywhere)
	USoundCue* LoopSoundRocket;

	UPROPERTY(EditAnywhere)
	USoundAttenuation* LoopAtt;

	UPROPERTY(VisibleAnywhere)
	URocketMovementComponent* RocketMovement;

private:
	UPROPERTY()
	UAudioComponent* StoreLoopSound;

	float SpawnTime = 0.f;
};
