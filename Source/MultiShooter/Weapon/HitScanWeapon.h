// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseWeapon.h"
#include "HitScanWeapon.generated.h"

class UParticleSystem;
class USoundCue;

UCLASS()
class MULTISHOOTER_API AHitScanWeapon : public ABaseWeapon
{
	GENERATED_BODY()
	
public:
	virtual void Fire(const FVector& HitTarget) override;

protected:
	void WeaponTraceHit(const FVector& TraceStart, const FVector& HitTarget, FHitResult& OutHit);

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactFx;

	UPROPERTY(EditAnywhere)
	USoundCue* HitSound;

	UPROPERTY(EditAnywhere)
	float DamageAmount = 20.f;

private:
	UPROPERTY(EditAnywhere)
	UParticleSystem* BeamFx;

	UPROPERTY(EditAnywhere)
	UParticleSystem* MuzzleFx;

	UPROPERTY(EditAnywhere)
	USoundCue* MuzzleSound;
};
