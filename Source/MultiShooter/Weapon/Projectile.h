// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

class UBoxComponent;
class UProjectileMovementComponent;
class UParticleSystem;
class USoundCue;
class UNiagaraSystem;
class UNiagaraComponent;

UCLASS()
class MULTISHOOTER_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	AProjectile();
	virtual void Tick(float DeltaTime) override;
	virtual void Destroyed() override;

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnHitCallBack(
		UPrimitiveComponent* HitComponent,
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		FVector NormalImpulse, 
		const FHitResult& Hit 
		);

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* ProjectileBody;

	UPROPERTY(EditAnywhere)
	float Damage = 20.f;

	UPROPERTY(EditAnywhere)
	float FallOfOuterRadiusDamage= 200.f;

	UPROPERTY(EditAnywhere)
	float InnerFullDamageRadius= 600.f;
		
	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactFX;

	UPROPERTY(EditAnywhere)
	USoundCue* ImpactSoundFX;

	UPROPERTY(EditAnywhere)
	UBoxComponent* CollisionBox;

	UPROPERTY(VisibleAnywhere)
	UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditAnywhere)
	UNiagaraSystem* Trail;

	UPROPERTY()
	UNiagaraComponent* TrailStore;

	void SpawnTrailSystem();
	void ExplodeDamage();
	void StartDestroyTimer();

	UFUNCTION()
	void DestroyTimerFinished();

	FTimerHandle DestroyTimer;
private:
	UPROPERTY(EditAnywhere)
	UParticleSystem* Tracer;

	UPROPERTY()
	UParticleSystemComponent* TracerComponent;	

	UPROPERTY(EditAnywhere)
	float DestroyTime = 5.f;

public:	
	

};
