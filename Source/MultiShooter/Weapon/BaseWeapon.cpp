// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseWeapon.h"
#include "Components/CapsuleComponent.h"
#include "Components/WidgetComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Net/UnrealNetwork.h"
#include "Casing.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Animation/AnimationAsset.h"
#include "MultiShooter/PlayerController/MultiPlayerController.h"
#include "MultiShooter/Character/MultiCharacter.h"
#include "MultiShooter/MultiComponents/CombatComponent.h"
#include "Kismet/KismetMathLibrary.h"

ABaseWeapon::ABaseWeapon()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
	SetReplicateMovement(true);

	WeapMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Body"));
	RootComponent = WeapMesh;
	WeapMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	WeapMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	WeapMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	WeapMesh->SetCustomDepthStencilValue(CUSTOM_DEPTH_BLUE);
	WeapMesh->MarkRenderStateDirty();
	EnableCustomDepth(true);

	PickupWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("PickupCOmponent"));
	PickupWidget->SetupAttachment(WeapMesh);

	CapsulePickup = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsulePuckup"));
	CapsulePickup->SetupAttachment(RootComponent);
	CapsulePickup->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	CapsulePickup->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ABaseWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABaseWeapon, WeaponState);
}

void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	
	CapsulePickup->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CapsulePickup->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	CapsulePickup->OnComponentBeginOverlap.AddDynamic(this, &ABaseWeapon::OnCapsuleBeginOverlapped);
	CapsulePickup->OnComponentEndOverlap.AddDynamic(this, &ABaseWeapon::OnCapsuleEndOverlapped);	

	if (PickupWidget)
	{
		PickupWidget->SetVisibility(false);
	}	
}

void ABaseWeapon::SetWeapState(EWeaponState State)
{
	WeaponState = State;
	OnWeaponStateSet();
}

void ABaseWeapon::OnWeaponStateSet()
{
	switch (WeaponState)
	{
		case EWeaponState::EWS_Equipped:

			OnEquipped();

		break;

		case EWeaponState::EWS_EquippedBack:

			OnEquippedBack();

		break;

		case EWeaponState::EWS_Dropped:

			OnDropped();

		break;
	}
}

void ABaseWeapon::OnRep_WeaponState()
{
	OnWeaponStateSet();
}

void ABaseWeapon::OnEquipped()
{
	ShowPickupWidget(false);
	CapsulePickup->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	WeapMesh->SetSimulatePhysics(false);
	WeapMesh->SetEnableGravity(false);
	WeapMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	if (WeaponType == EWeaponType::EWT_SubmachinGun)
	{
		WeapMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		WeapMesh->SetEnableGravity(true);
		WeapMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	}

	EnableCustomDepth(false);
}

void ABaseWeapon::OnDropped()
{
	if (HasAuthority())
	{
		CapsulePickup->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}

	WeapMesh->SetSimulatePhysics(true);
	WeapMesh->SetEnableGravity(true);
	WeapMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	WeapMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	WeapMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	WeapMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);

	WeapMesh->SetCustomDepthStencilValue(CUSTOM_DEPTH_BLUE);
	WeapMesh->MarkRenderStateDirty();
	EnableCustomDepth(true);
}

void ABaseWeapon::OnEquippedBack()
{
	ShowPickupWidget(false);
	CapsulePickup->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	WeapMesh->SetSimulatePhysics(false);
	WeapMesh->SetEnableGravity(false);
	WeapMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	if (WeaponType == EWeaponType::EWT_SubmachinGun)
	{
		WeapMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		WeapMesh->SetEnableGravity(true);
		WeapMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	}

	EnableCustomDepth(false);
}

void ABaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseWeapon::OnCapsuleBeginOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AMultiCharacter* Char = Cast<AMultiCharacter>(OtherActor);
	if (Char)
	{
		Char->SetOverlappingWeapon(this);
	}
}

void ABaseWeapon::OnCapsuleEndOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AMultiCharacter* Char = Cast<AMultiCharacter>(OtherActor);
	if (Char)
	{
		Char->SetOverlappingWeapon(nullptr);
	}
}

void ABaseWeapon::ShowPickupWidget(bool bShowWidget)
{
	if (PickupWidget)
	{
		PickupWidget->SetVisibility(bShowWidget);
	}
}

void ABaseWeapon::Fire(const FVector& HitTarget)
{
	if (FireAnimation)
	{
		WeapMesh->PlayAnimation(FireAnimation, false);
	}

	if (CasingClass)
	{
		const USkeletalMeshSocket* EjectSocket = WeapMesh->GetSocketByName(FName("AmmoEject"));
		if (EjectSocket)
		{
			FTransform SocketTransform = EjectSocket->GetSocketTransform(WeapMesh);
			if (GetWorld())
			{
				GetWorld()->SpawnActor<ACasing>(
					CasingClass, 
					SocketTransform.GetLocation(),
					SocketTransform.GetRotation().Rotator()
					);
			}
		}
	}
	SpendRound();
}

FVector ABaseWeapon::TraceEndWithScatter(const FVector& HitTarget)
{
	const USkeletalMeshSocket* MuzzleFlashSocket = GetWeaponMesh()->GetSocketByName(FName("MuzzleFlash"));
	if (!MuzzleFlashSocket) return FVector();

		const FTransform MuzzleSocketTransform = MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
		const FVector TraceStart = MuzzleSocketTransform.GetLocation();

		const FVector ToTargetNormalized = (HitTarget - TraceStart).GetSafeNormal();
		const FVector SphereCenter = TraceStart + ToTargetNormalized * DistanceToSphere;
		const FVector RandVec = UKismetMathLibrary::RandomUnitVector() * FMath::FRandRange(0.f, SpreadRadius);
		const FVector EndLoc = SphereCenter + RandVec;
		const FVector ToEndLoc = EndLoc - TraceStart;
///////////////////////////////////////////////////////
// 	   DEBUG TRACERS FOR SPREAD
// 
	//DrawDebugSphere(
	//	GetWorld(),
	//	SphereCenter,
	//	SpreadRadius,
	//	12,
	//	FColor::Red,
	//	true
	//);

	//DrawDebugSphere(
	//	GetWorld(),
	//	EndLoc,
	//	5.f,
	//	12,
	//	FColor::Yellow,
	//	true
	//);

	//DrawDebugLine(
	//	GetWorld(),
	//	TraceStart,
	//	FVector(TraceStart + ToEndLoc * TRACE_LENGTH / ToEndLoc.Size()),
	//	FColor::Green,
	//	true
	//);

	return FVector(TraceStart + ToEndLoc * TRACE_LENGTH / ToEndLoc.Size());
}

void ABaseWeapon::Dropped()
{
	SetWeapState(EWeaponState::EWS_Dropped);

	FDetachmentTransformRules DetachRules(EDetachmentRule::KeepWorld, true);
	WeapMesh->DetachFromComponent(DetachRules);
	SetOwner(nullptr);
	MultiOwnerCharacter = nullptr;
	MultiOwnerController = nullptr;
}

void ABaseWeapon::SetHUDAmmo()
{
	if (GetOwner())
	{
		MultiOwnerCharacter = MultiOwnerCharacter == nullptr ? Cast<AMultiCharacter>(GetOwner()) : MultiOwnerCharacter;
		if (MultiOwnerCharacter)
		{
			MultiOwnerController = MultiOwnerController == nullptr ? Cast<AMultiPlayerController>(MultiOwnerCharacter->Controller) : MultiOwnerController;
			if (MultiOwnerController)
			{
				MultiOwnerController->SetHUDWeaponAmmo(Ammo);
			}
		}
	}
}

void ABaseWeapon::OnRep_Owner()
{
	Super::OnRep_Owner();	

	if (!Owner)
	{
		MultiOwnerCharacter = nullptr;
		MultiOwnerController = nullptr;
	}
	else
	{
		MultiOwnerCharacter = MultiOwnerCharacter == nullptr ? Cast<AMultiCharacter>(Owner) : MultiOwnerCharacter;
		if (MultiOwnerCharacter && MultiOwnerCharacter->GetEquippedWeapon() && MultiOwnerCharacter->GetEquippedWeapon() == this)
		{
			SetHUDAmmo();
		}
	}
}

void ABaseWeapon::SpendRound()
{
	Ammo = FMath::Clamp(Ammo - 1, 0, MagCapacity);
	SetHUDAmmo();
	
	if (HasAuthority())
	{
		ClientUpdateAmmo(Ammo);
	}
	else if (MultiOwnerCharacter && MultiOwnerCharacter->IsLocallyControlled())
	{
		++ AmmoSequence;
	}
}

void ABaseWeapon::ClientUpdateAmmo_Implementation(int32 ServerAmmo)
{
	if(HasAuthority()) return;

		Ammo = ServerAmmo;
		--AmmoSequence;
		Ammo -= AmmoSequence;
		SetHUDAmmo();
}

void ABaseWeapon::ClientAddAmmo_Implementation(int32 AmmoToAdd)
{
	if(HasAuthority()) return;

		Ammo = FMath::Clamp(Ammo + AmmoToAdd, 0, MagCapacity);

		MultiOwnerCharacter = MultiOwnerCharacter == nullptr ? Cast<AMultiCharacter>(GetOwner()) : MultiOwnerCharacter;
		if (MultiOwnerCharacter && MultiOwnerCharacter->GetCombat() && IsFull())
		{
			MultiOwnerCharacter->GetCombat()->JumpToShotGunEnd();
		}

		SetHUDAmmo();
}

void ABaseWeapon::AddAmmo(int32 AmmoToAdd)
{
	Ammo = FMath::Clamp(Ammo + AmmoToAdd, 0, MagCapacity);
	SetHUDAmmo();

	ClientAddAmmo(AmmoToAdd);
}

bool ABaseWeapon::IsEmpty()
{
	return Ammo <= 0; 
}

bool ABaseWeapon::IsFull()
{
	return Ammo == MagCapacity;
}

void ABaseWeapon::EnableCustomDepth(bool bEnable)
{
	if (WeapMesh)
	{
		WeapMesh->SetRenderCustomDepth(bEnable);
	}
}