// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileRocket.h"
#include "TimerManager.h"
#include "Components/BoxComponent.h"
//#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Sound/SoundCue.h"
#include "Sound/SoundAttenuation.h"
#include "Components/AudioComponent.h"
#include "RocketMovementComponent.h"
#include "Kismet/GameplayStatics.h"

AProjectileRocket::AProjectileRocket()
{
	ProjectileBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Rocket body"));
	ProjectileBody->SetupAttachment(RootComponent);
	ProjectileBody->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	RocketMovement = CreateDefaultSubobject<URocketMovementComponent>(TEXT("Rocket Movement"));
	RocketMovement->bRotationFollowsVelocity = true;
	RocketMovement->SetIsReplicated(true);
}

void AProjectileRocket::BeginPlay()
{
	Super::BeginPlay();

	SpawnTime = GetWorld()->GetTimeSeconds();

	if (GetWorld()->GetTimeSeconds() - SpawnTime > 10.f)
	{
		Destroy();
	}

	if (!HasAuthority())
	{
		CollisionBox->OnComponentHit.AddDynamic(this, &AProjectileRocket::OnHitCallBack);
	}

	SpawnTrailSystem();

	if (LoopSoundRocket && LoopAtt)
	{
		StoreLoopSound = UGameplayStatics::SpawnSoundAttached(
			LoopSoundRocket,
			GetRootComponent(),
			FName(),
			GetActorLocation(),
			EAttachLocation::KeepWorldPosition,
			false,
			1.f,
			1.f,
			0.f,
			LoopAtt,
			(USoundConcurrency*)nullptr,
			false
		);
	}
}

void AProjectileRocket::OnHitCallBack(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if(OtherActor == GetOwner()) return;

		ExplodeDamage();
		//Super::OnHitCallBack(HitComponent,OtherActor,OtherComp,NormalImpulse,Hit);  // No need destroy, because we destroy actor by timer

		// Make delay for destroy
		StartDestroyTimer();

		// Fake Destroy
		if (TrailStore && TrailStore->GetSystemInstance())
		{
			TrailStore->GetSystemInstance()->Deactivate();
		}

		if (ProjectileBody)
		{
			ProjectileBody->SetVisibility(false);
		}

		if (CollisionBox)
		{
			CollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}

		if (StoreLoopSound && StoreLoopSound->IsPlaying())
		{
			StoreLoopSound->Stop();
		}

		// Imact Fx
		if (ImpactFX)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),ImpactFX,GetActorTransform());
		}

		if (ImpactSoundFX)
		{
			UGameplayStatics::PlaySoundAtLocation(this,ImpactSoundFX,GetActorLocation());
		}
}

void AProjectileRocket::Destroyed()
{

}