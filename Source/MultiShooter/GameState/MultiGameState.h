// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "MultiGameState.generated.h"

class AMultiPlayerState;

UCLASS()
class MULTISHOOTER_API AMultiGameState : public AGameState
{
	GENERATED_BODY()

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const override;
	void UpdateTopScore(AMultiPlayerState* ScoringPlayer);

	UPROPERTY(Replicated)
	TArray<AMultiPlayerState*> TopScoringPlayers;

private:

	float TopScore = 0.f;
};
