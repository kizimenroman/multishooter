// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BuffComponent.generated.h"

class AMultiCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MULTISHOOTER_API UBuffComponent : public UActorComponent
{
	GENERATED_BODY()
/////////////////////////////////////
/*
*	Base Setups
*/
public:	
	UBuffComponent();
	friend AMultiCharacter;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
protected:
	virtual void BeginPlay() override;
private:
	UPROPERTY()
	AMultiCharacter* Character;	
/////////////////////////////////////
/*
*	HEAL
*/
public:
	void Heal(float HealAmount, float HealTime);
protected:
	void HealRampUp(float DeltaTime);
private:
	bool bHealing = false;
	float AmountToHeal = 0.f;
	float HealingRate = 0.f;
/////////////////////////////////////
/*
*	SHIELD
*/
public:
	void ReplenishShield(float AmountReplanish, float TimeReplanish);
protected:
	void ShieldRampUp(float DeltaTime);
private:
	bool bReplenishing = false;
	float AmountToReplenish = 0.f;
	float ReplenishRate = 0.f;
/////////////////////////////////////
/*
*	SPEED
*/
public:
	void SetInitialSpeeds(float DefaultStandSpeedToSet, float DefaultCrouchSpeedToSet);
	void BuffSpeed(float BaseBuff, float CrouchBuff, float TimeBuff);
protected:
	//
private:	
	FTimerHandle SpeedBuffTimer;
	float DefaultStandSpeed;
	float DefaultCrouchSpeed;

	UFUNCTION(NetMulticast, Reliable)
	void MultiCastSpeedBuff(float StandBaseSpeed, float CrouchBaseSpeed);

	void ResetSpeeds();
/////////////////////////////////////
/*
*	JUMP
*/
public:
	void SetDefaultJumpVelocity(float DefaultVelocity);
	void BuffJump(float BuffJumpVelocity, float BuffTimeVelocity);
protected:
	//
private:
	FTimerHandle JumpBuffTimer;
	float DefaultJumpVelocity;

	void ResetJump();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastJumpBuff(float JumpVelocity);
/////////////////////////////////////
};
