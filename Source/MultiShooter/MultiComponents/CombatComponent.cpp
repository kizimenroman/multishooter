// Fill out your copyright notice in the DesScription page of Project Settings.


#include "CombatComponent.h"
#include "MultiShooter/Character/MultiCharacter.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Net/UnrealNetwork.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "MultiShooter/PlayerController/MultiPlayerController.h"
#include "Camera/CameraComponent.h"
#include "TimerManager.h"
#include "Sound/SoundCue.h"
#include "MultiShooter/Weapon/BaseWeapon.h"
#include "MultiShooter/Weapon/Projectile.h"
#include "MultiShooter/Weapon/Shotgun.h"
#include "MultiShooter/Character/MultiAnimInstance.h"

UCombatComponent::UCombatComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	BaseWalkSpeed = 650.0f;
	AimWalkSpeed = 450.0f;
}

void UCombatComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCombatComponent, EquippedWeapon);
	DOREPLIFETIME(UCombatComponent, IsAiming);
	DOREPLIFETIME_CONDITION(UCombatComponent, CarriedAmmo, COND_OwnerOnly);
	DOREPLIFETIME(UCombatComponent, CombatState);
	DOREPLIFETIME(UCombatComponent, Grenades);
	DOREPLIFETIME(UCombatComponent, SecondWeapon);
}

void UCombatComponent::BeginPlay()
{
	Super::BeginPlay();	

	if (Character)
	{
		Character->GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed;

		if (Character->GetCameraComponent())
		{
			DefaultFOV = Character->GetCameraComponent()->FieldOfView;
			CurrentFOV = DefaultFOV;	
		}
		if (Character->HasAuthority())
		{
			InitializeCarriedAmmo();
		}
	}
}

void UCombatComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
		
	if (Character && Character->IsLocallyControlled())
	{
		FHitResult HitResult;
		TraceUnderCrosshairs(HitResult);
		HitTarget = HitResult.ImpactPoint;

		SetHUDCrosshairs(DeltaTime);
		InterpFOV(DeltaTime);
	}
}

void UCombatComponent::OnRep_CombatState()
{
	switch (CombatState)
	{
		case ECombatState::ECS_Reloading:
			if (Character && !Character->IsLocallyControlled()) HandleReload();
			break;

		case ECombatState::ECS_Unoccupied:
			if (bFireButtonPressed)
			{
				Fire();
			}
			break;

		case ECombatState::ECS_ThrowingGrenade:
			if (Character && !Character->IsLocallyControlled())
			{
				Character->PlayThrowGrenadeMontage();
				AttachActorToLeftHand(EquippedWeapon);
				ShowAttachedGrenade(true);
			}
			break;
	}
}

void UCombatComponent::DoEquipWeapon(ABaseWeapon* WeaponToEquip)
{
	if(!Character || !WeaponToEquip) return;
	if(CombatState != ECombatState::ECS_Unoccupied) return;
		
		if(EquippedWeapon && !SecondWeapon)
		{
			EquipSecondWeapon(WeaponToEquip);		
		}
		else
		{
			EquipPrimaryWeapon(WeaponToEquip);
		}


		Character->GetCharacterMovement()->bOrientRotationToMovement = false;
		Character->bUseControllerRotationYaw = true;
}

void UCombatComponent::SwapWeapons()
{
	if(CombatState != ECombatState::ECS_Unoccupied) return;

		ABaseWeapon* TempWeapon = EquippedWeapon;
		EquippedWeapon = SecondWeapon;
		SecondWeapon = TempWeapon;

		// New primary handle
		EquippedWeapon->SetWeapState(EWeaponState::EWS_Equipped);
		AttachActorToRightHand(EquippedWeapon);
		EquippedWeapon->SetHUDAmmo();
		UpdateCarriedAmmo();
		PlayEquipWeaponSound(EquippedWeapon);

		// New back handle
		SecondWeapon->SetWeapState(EWeaponState::EWS_EquippedBack);
		AttachActorToBackPack(SecondWeapon);
}

void UCombatComponent::EquipPrimaryWeapon(ABaseWeapon* WeaponToEquip)
{
	if(!WeaponToEquip) return;

		DropEquipWeapon();

		EquippedWeapon = WeaponToEquip;
		EquippedWeapon->SetWeapState(EWeaponState::EWS_Equipped);
		AttachActorToRightHand(EquippedWeapon);
		EquippedWeapon->SetOwner(Character);

		EquippedWeapon->SetHUDAmmo();
		UpdateCarriedAmmo();

		PlayEquipWeaponSound(WeaponToEquip);

		AutoReloadCheck();
}

void UCombatComponent::OnRep_EquippedWeapon()
{
	if (EquippedWeapon && Character)
	{
		EquippedWeapon->SetWeapState(EWeaponState::EWS_Equipped);
		AttachActorToRightHand(EquippedWeapon);
		PlayEquipWeaponSound(EquippedWeapon);
		EquippedWeapon->SetHUDAmmo();

		Character->GetCharacterMovement()->bOrientRotationToMovement = false;
		Character->bUseControllerRotationYaw = true;
	}
}

void UCombatComponent::EquipSecondWeapon(ABaseWeapon* WeaponToEquip)
{
	if(!WeaponToEquip) return;

		SecondWeapon = WeaponToEquip;
		SecondWeapon->SetWeapState(EWeaponState::EWS_EquippedBack);
		AttachActorToBackPack(WeaponToEquip);
		SecondWeapon->SetOwner(Character);
		PlayEquipWeaponSound(WeaponToEquip);
}

void UCombatComponent::OnRep_SecondWeapon()
{
	if (SecondWeapon && Character)
	{
		SecondWeapon->SetWeapState(EWeaponState::EWS_EquippedBack);
		AttachActorToBackPack(SecondWeapon);
		PlayEquipWeaponSound(SecondWeapon);
	}
}

void UCombatComponent::AttachActorToRightHand(AActor* ActorToAttach)
{	
	if(!Character || !ActorToAttach) return;

		const USkeletalMeshSocket* HandSocket = Character->GetMesh()->GetSocketByName(FName("WeaponSocket"));
		if (HandSocket)
		{
			HandSocket->AttachActor(ActorToAttach, Character->GetMesh());
		}
}

void UCombatComponent::AttachActorToLeftHand(AActor* ActorToAttach)
{
	if(!Character || !ActorToAttach || !EquippedWeapon) return;
		
		bool bUsePistolSocket = 
			EquippedWeapon->GetWeaponType() == EWeaponType::EWT_Pistol ||
			EquippedWeapon->GetWeaponType() == EWeaponType::EWT_SubmachinGun;
		FName SocketName = bUsePistolSocket ? FName("LeftHandSocketPistol") : FName("LeftHandSocket");

		const USkeletalMeshSocket* HandSocket = Character->GetMesh()->GetSocketByName(SocketName);
		if (HandSocket && Character->GetMesh())
		{
			HandSocket->AttachActor(ActorToAttach, Character->GetMesh());
		}
}

void UCombatComponent::AttachActorToBackPack(AActor* ActorToAttach)
{
	if(!Character || !ActorToAttach) return;

		const USkeletalMeshSocket* Backpack = Character->GetMesh()->GetSocketByName(FName("BackpackSocket"));
		if (Backpack && Character->GetMesh())
		{
			Backpack->AttachActor(ActorToAttach, Character->GetMesh());
		}
}

void UCombatComponent::UpdateCarriedAmmo()
{
	if(!EquippedWeapon) return;

		if (CarriedAmmoMap.Contains(EquippedWeapon->GetWeaponType()))
		{
			CarriedAmmo = CarriedAmmoMap[EquippedWeapon->GetWeaponType()];
		}

		MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(Character->Controller) : MultiController;
		if (MultiController)
		{
			MultiController->SetHUDCarriedAmmo(CarriedAmmo);
		}
}

void UCombatComponent::PlayEquipWeaponSound(ABaseWeapon* Weapon)
{
	if (Character && Weapon && Weapon->EquipSFX)
	{
		UGameplayStatics::PlaySoundAtLocation(
			this,
			Weapon->EquipSFX,
			Character->GetActorLocation()
		);
	}
}

void UCombatComponent::AutoReloadCheck()
{
	if (EquippedWeapon && EquippedWeapon->IsEmpty())
	{
		Reload();
	}
}

void UCombatComponent::DropEquipWeapon()
{
	if (EquippedWeapon)
	{
		EquippedWeapon->Dropped();
	}
}

void UCombatComponent::Reload()
{
	if (CarriedAmmo > 0 && CombatState == ECombatState::ECS_Unoccupied && EquippedWeapon && !EquippedWeapon->IsFull() && !bLocallyReloading)
	{
		ServerReload();
		HandleReload();
		bLocallyReloading = true;
	}
}

void UCombatComponent::ServerReload_Implementation()
{
	if (!Character) return;

	CombatState = ECombatState::ECS_Reloading;
	if (!Character->IsLocallyControlled()) HandleReload();
}

void UCombatComponent::HandleReload()
{
	if (Character)
	{
		Character->PlayReloadMontage();
	}
}

void UCombatComponent::FinishReloading()
{
	if(Character == nullptr) return;

		bLocallyReloading = false;

		if (Character->HasAuthority())
		{
			CombatState = ECombatState::ECS_Unoccupied;
			UpdateAmmoValues();
		}
		if (bFireButtonPressed)
		{
			Fire();
		}
}

void UCombatComponent::UpdateAmmoValues()
{
	if (!Character || !EquippedWeapon) return;

		int32 ReloadAmount = AmountToReload();
		if (CarriedAmmoMap.Contains(EquippedWeapon->GetWeaponType()))
		{
			CarriedAmmoMap[EquippedWeapon->GetWeaponType()] -= ReloadAmount;
			CarriedAmmo = CarriedAmmoMap[EquippedWeapon->GetWeaponType()];
		}

		MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(Character->Controller) : MultiController;
		if (MultiController)
		{
			MultiController->SetHUDCarriedAmmo(CarriedAmmo);
		}

		EquippedWeapon->AddAmmo(ReloadAmount);
}

int32 UCombatComponent::AmountToReload()
{
	if(!EquippedWeapon) return 0;
	
	int32 RoomInMag = EquippedWeapon->GetMagCapacity() - EquippedWeapon->GetAmmo();

	if (CarriedAmmoMap.Contains(EquippedWeapon->GetWeaponType()))
	{
		int32 AmountCarried = CarriedAmmoMap[EquippedWeapon->GetWeaponType()];
		int32 Least = FMath::Min(RoomInMag, AmountCarried);
		return FMath::Clamp(RoomInMag, 0 , Least);
	}
	return 0;
}

void UCombatComponent::ShotgunShellReload()
{
	if (Character && Character->HasAuthority())
	{
		UpdateShotgunAmmoValues();
	}
}

void UCombatComponent::UpdateShotgunAmmoValues()
{
	if (!Character || !EquippedWeapon) return;

		if (CarriedAmmoMap.Contains(EquippedWeapon->GetWeaponType()))
		{
			CarriedAmmoMap[EquippedWeapon->GetWeaponType()] -= 1;
			CarriedAmmo = CarriedAmmoMap[EquippedWeapon->GetWeaponType()];
		}

		MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(Character->Controller) : MultiController;
		if (MultiController)
		{
			MultiController->SetHUDCarriedAmmo(CarriedAmmo);
		}

		EquippedWeapon->AddAmmo(1);
		bCanFire = true;

		if (EquippedWeapon->IsFull() || CarriedAmmo == 0)
		{
			JumpToShotGunEnd();
		}
}

bool UCombatComponent::ShouldSwapWeapons()
{
	return (EquippedWeapon && SecondWeapon);
}

void UCombatComponent::JumpToShotGunEnd()
{
	UAnimInstance* AnimInstance = Character->GetMesh()->GetAnimInstance();
	if (AnimInstance && Character->GetReloadMontage())
	{
		AnimInstance->Montage_JumpToSection(FName("ShotgunEnd"));
	}
}

void UCombatComponent::SetHUDCrosshairs(float DeltaTime)
{
	if(!Character && !Character->Controller) return;

	MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(Character->Controller) : MultiController;
	if (MultiController)
	{
		HUD = HUD == nullptr ? Cast<AMultiHUD>(MultiController->GetHUD()) : HUD;
		if (HUD)
		{
			if (EquippedWeapon)
			{
				HUDPackage.CrosshairsCentr = EquippedWeapon->CrossHairCentr;
				HUDPackage.CrosshairsLeft = EquippedWeapon->CrosshairLeft;
				HUDPackage.CrosshairsRight= EquippedWeapon->CrosshairRight;
				HUDPackage.CrosshairsBottom = EquippedWeapon->CrosshairBottom;
				HUDPackage.CrosshairsTop = EquippedWeapon->CrosshairTop;
			}
			else
			{
				HUDPackage.CrosshairsCentr = nullptr;
				HUDPackage.CrosshairsLeft = nullptr;
				HUDPackage.CrosshairsRight= nullptr;
				HUDPackage.CrosshairsBottom = nullptr;
				HUDPackage.CrosshairsTop = nullptr;
			}
				FVector2D WalkSpeedRange(0.f, Character->GetCharacterMovement()->MaxWalkSpeed);
				FVector2D VelocityMultiplayerRange(0.f,1.f);
				FVector Velocity =  Character->GetVelocity();
				Velocity.Z = 0.f;
				CrosshairVelocityFactor = FMath::GetMappedRangeValueClamped(WalkSpeedRange, VelocityMultiplayerRange, Velocity.Size());

				if (Character->GetCharacterMovement()->IsFalling())
				{
					CrosshairInAirFactor = FMath::FInterpTo(CrosshairInAirFactor, 2.25f, DeltaTime, 2.25f);
				}
				else
				{
					CrosshairInAirFactor = FMath::FInterpTo(CrosshairInAirFactor, 0.f, DeltaTime, 30.f);
				}

				if (IsAiming && EquippedWeapon)
				{
					CrosshairAimFactor = FMath::FInterpTo(CrosshairAimFactor, 0.58f, DeltaTime, EquippedWeapon->GetZoomInterpSpeed());
				}
				else if(!IsAiming && EquippedWeapon)
				{
					CrosshairAimFactor = FMath::FInterpTo(CrosshairAimFactor, 0.f, DeltaTime, EquippedWeapon->GetZoomInterpSpeed());
				}

				CrosshairShootingFactor = FMath::FInterpTo(CrosshairShootingFactor, 0, DeltaTime, 4.f);

				HUDPackage.CrosshairSpread = 
					0.5f +
					CrosshairVelocityFactor + 
					CrosshairInAirFactor - 
					CrosshairAimFactor + 
					CrosshairShootingFactor;

				HUD->SetHUDPackage(HUDPackage);
		}
	}
}

void UCombatComponent::TraceUnderCrosshairs(FHitResult& TraceHitResult)
{
	FVector2D ViewportSize;

	if (GEngine && GEngine->GameViewport)
	{
		GEngine->GameViewport->GetViewportSize(ViewportSize);
	}

	FVector2D CrosshairLocation(ViewportSize.X * 0.5f, ViewportSize.Y * 0.5f);
	FVector CrosshairWorldPosition;
	FVector CrosshairWorldDirection;
	bool bScreenToWorld =  UGameplayStatics::DeprojectScreenToWorld(
		UGameplayStatics::GetPlayerController(this, 0),
		CrosshairLocation,
		CrosshairWorldPosition,
		CrosshairWorldDirection
	);
	
	if (bScreenToWorld)
	{
		FVector Start = CrosshairWorldPosition;
		if (Character)
		{
			float DistanceToCharacter = (Character->GetActorLocation() - Start).Size();
			Start += CrosshairWorldDirection * (DistanceToCharacter + 50.f);
		}

		FVector End = Start + CrosshairWorldDirection * TRACE_LENGTH;

		GetWorld()->LineTraceSingleByChannel(
			TraceHitResult,
			Start,
			End,
			ECollisionChannel::ECC_Visibility
		);

		if (!TraceHitResult.bBlockingHit)
		{
			TraceHitResult.ImpactPoint = End;
		}

		if (TraceHitResult.GetActor() && TraceHitResult.GetActor()->Implements<UInterractWithCrosshairsInterface>())
		{
			HUDPackage.CrosshairsColor = FLinearColor::Red;
		}
		else
		{
			HUDPackage.CrosshairsColor = FLinearColor::White;
		}
	}
}

void UCombatComponent::SetAiming(bool bIsAiming)
{
	if(!Character || !EquippedWeapon) return;

		IsAiming = bIsAiming;
		ServerSetAiming(bIsAiming);				
		Character->GetCharacterMovement()->MaxWalkSpeed = bIsAiming ? AimWalkSpeed : BaseWalkSpeed;
		
		if (Character->IsLocallyControlled() && EquippedWeapon->GetWeaponType() == EWeaponType::EWT_SniperRifle)
		{
			Character->ShowSniperScopeWidget(bIsAiming);
		}

		if (Character->IsLocallyControlled()) bAimButtonPressed  = bIsAiming;
}

void UCombatComponent::ServerSetAiming_Implementation(bool bIsAiming)
{
	IsAiming = bIsAiming;

	if (Character)
	{
		Character->GetCharacterMovement()->MaxWalkSpeed = bIsAiming ? AimWalkSpeed : BaseWalkSpeed;
	}
}

void UCombatComponent::OnRep_Aiming()
{
	if (Character && Character->IsLocallyControlled())
	{
		IsAiming = bAimButtonPressed;
	}
}

void UCombatComponent::InterpFOV(float DeltaTime)
{
	if(!EquippedWeapon) return;

	if (IsAiming)
	{
		CurrentFOV = FMath::FInterpTo(CurrentFOV, EquippedWeapon->GetZoomedFOV(), DeltaTime, EquippedWeapon->GetZoomInterpSpeed());
	}
	else
	{
		CurrentFOV = FMath::FInterpTo(CurrentFOV, DefaultFOV, DeltaTime, ZoomInterpSpeed);
	}

	if (Character && Character->GetCameraComponent())
	{
		Character->GetCameraComponent()->SetFieldOfView(CurrentFOV);
	}
}

void UCombatComponent::FireButtonPressed(bool bPressed)
{
	bFireButtonPressed = bPressed;

	if(bFireButtonPressed)
	{
		Fire();
	}
}

bool UCombatComponent::CanFire()
{
	if (!EquippedWeapon) return false;
	if (bLocallyReloading) return false;

		bool bShotgunInterrapt = 
			!EquippedWeapon->IsEmpty() && 
			bCanFire && 
			CombatState == ECombatState::ECS_Reloading && 
			EquippedWeapon->GetWeaponType() == EWeaponType::EWT_Shotgun;

		if (bShotgunInterrapt)
		{
			return true;
		}

		return !EquippedWeapon->IsEmpty() && 
				bCanFire && 
				CombatState == ECombatState::ECS_Unoccupied;
}

void UCombatComponent::Fire()
{
	if (CanFire())
	{
		bCanFire = false;
		
		if (EquippedWeapon)
		{
			switch (EquippedWeapon->FireType)
			{
			case EFireType::EFT_HitScanWeapon:

				FireHitScanWeapon();

				break;
			case EFireType::EFT_ProjectileWeapon:

				FireProjectileWeapon();

				break;
			case EFireType::EFT_Shotgun:

				FireShotgun();

				break;
			case EFireType::EFT_MAX:
				break;
			default:
				break;
			}		
			CrosshairShootingFactor = EquippedWeapon->SpreadFire;
		}
		StartFireTimer();
	}
}

void UCombatComponent::FireProjectileWeapon()
{
	if (EquippedWeapon && Character)
	{
		HitTarget = EquippedWeapon->bUseScatter ? EquippedWeapon->TraceEndWithScatter(HitTarget) : HitTarget;

		if (!Character->HasAuthority()) LocalFire(HitTarget);
		ServerFire(HitTarget);
	}
}

void UCombatComponent::FireHitScanWeapon()
{
	if (EquippedWeapon && Character)
	{
		HitTarget = EquippedWeapon->bUseScatter ? EquippedWeapon->TraceEndWithScatter(HitTarget) : HitTarget;

		if (!Character->HasAuthority()) LocalFire(HitTarget);
		ServerFire(HitTarget);
	}
}

void UCombatComponent::FireShotgun()
{
	AShotgun* Shotgun = Cast<AShotgun>(EquippedWeapon);
	if (Shotgun && Character)
	{
		TArray<FVector_NetQuantize> HitTargets;
		Shotgun->ShotgunTraceWithScatter(HitTarget, HitTargets);
		if (!Character->HasAuthority()) ShotgunLocalFire(HitTargets);
		ServerShotgunFire(HitTargets);
	}
}

void UCombatComponent::ServerFire_Implementation(const FVector_NetQuantize& TraceHitTarget)
{
	MulticastFire(TraceHitTarget);
}

void UCombatComponent::MulticastFire_Implementation(const FVector_NetQuantize& TraceHitTarget)
{
	if (Character && Character->IsLocallyControlled() && !Character->HasAuthority()) return;

		LocalFire(TraceHitTarget);
}

void UCombatComponent::ServerShotgunFire_Implementation(const TArray<FVector_NetQuantize>& TraceHitTargets)
{
	MulticastShotgunFire(TraceHitTargets);
}

void UCombatComponent::MulticastShotgunFire_Implementation(const TArray<FVector_NetQuantize>& TraceHitTargets)
{
	if (Character && Character->IsLocallyControlled() && !Character->HasAuthority()) return;

		ShotgunLocalFire(TraceHitTargets);
}

void UCombatComponent::LocalFire(const FVector_NetQuantize& TraceHitTarget)
{
	if(!EquippedWeapon) return;

		if (Character && CombatState == ECombatState::ECS_Unoccupied)
		{
			Character->PlayFireMontage(IsAiming);
			EquippedWeapon->Fire(TraceHitTarget);
		}
}

void UCombatComponent::ShotgunLocalFire(const TArray<FVector_NetQuantize>& TraceHitTargets)
{
	AShotgun* Shotgun = Cast<AShotgun>(EquippedWeapon);
	if (!Shotgun || !Character) return;
			
		if (CombatState == ECombatState::ECS_Reloading || CombatState == ECombatState::ECS_Unoccupied)
		{
			Character->PlayFireMontage(IsAiming);
			Shotgun->FireShotgun(TraceHitTargets);
			CombatState = ECombatState::ECS_Unoccupied;
		}
}

void UCombatComponent::StartFireTimer()
{
	if(!Character || !EquippedWeapon) return;

	Character->GetWorldTimerManager().SetTimer(
		FireTimer,
		this,
		&UCombatComponent::FinishedFireTimer,
		EquippedWeapon->FireDelay
	);
}

void UCombatComponent::FinishedFireTimer()
{
	if(!EquippedWeapon) return;

		bCanFire = true;
		if (bFireButtonPressed && EquippedWeapon->bAutomatic)
		{
			Fire();
		}

		AutoReloadCheck();
}

void UCombatComponent::ThrowGrenade()
{
	if(Grenades == 0) return;
	if(CombatState != ECombatState::ECS_Unoccupied || !EquippedWeapon) return;

		CombatState = ECombatState::ECS_ThrowingGrenade;

		if (Character && EquippedWeapon)
		{
			Character->PlayThrowGrenadeMontage();
			AttachActorToLeftHand(EquippedWeapon);
			ShowAttachedGrenade(true);
		}

		if (Character && !Character->HasAuthority())
		{
			ServerThrowGrenade();
		}

		if (Character && Character->HasAuthority())
		{
			Grenades = FMath::Clamp(Grenades -1, 0, MaxGrenades);
			UpdateHUDGrenades();
		}
}

void UCombatComponent::ServerThrowGrenade_Implementation()
{
	if(Grenades == 0) return;
	CombatState = ECombatState::ECS_ThrowingGrenade;

	if (Character)
	{
		Character->PlayThrowGrenadeMontage();
		AttachActorToLeftHand(EquippedWeapon);
		ShowAttachedGrenade(true);
	}
	Grenades = FMath::Clamp(Grenades -1, 0, MaxGrenades);
	UpdateHUDGrenades();
}

void UCombatComponent::LaunchGrenade()
{
	ShowAttachedGrenade(false);

	if (Character && Character->IsLocallyControlled())
	{
		ServerLaunchGrenade(HitTarget);
	}
}

void UCombatComponent::ServerLaunchGrenade_Implementation(const FVector_NetQuantize& Target)
{
	if (Character && GrenadeClass && Character->GetAttachedGrenade())
	{
		const FVector SpawnLocation = Character->GetAttachedGrenade()->GetComponentLocation();
		FVector ToTarget = Target - SpawnLocation;
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = Character;
		SpawnParams.Instigator = Character;

		if (GetWorld())
		{
			GetWorld()->SpawnActor<AProjectile>(
				GrenadeClass,
				SpawnLocation,
				ToTarget.Rotation(),
				SpawnParams
			);
		}
	}
}

void UCombatComponent::ThrowGrenadeFinished()
{
	CombatState = ECombatState::ECS_Unoccupied;

	if (EquippedWeapon)
	{
		AttachActorToRightHand(EquippedWeapon);
	}
}

void UCombatComponent::UpdateHUDGrenades()
{
	if(!Character) return;

		MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(Character->GetController()) : MultiController;
		if (MultiController)
		{
			MultiController->SetHUDGrenades(Grenades);
		}
}

void UCombatComponent::ShowAttachedGrenade(bool bShow)
{
	if (Character && Character->GetAttachedGrenade())
	{
		Character->GetAttachedGrenade()->SetVisibility(bShow);
	}
}

void UCombatComponent::OnRep_CarriedAmmo()
{
	MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(Character->Controller) : MultiController;
	if (MultiController)
	{
		MultiController->SetHUDCarriedAmmo(CarriedAmmo);
	}

	bool bJumpToShotgunEnd = 
		CarriedAmmo == 0 &&
		CombatState == ECombatState::ECS_Reloading &&
		EquippedWeapon &&
		EquippedWeapon->GetWeaponType() == EWeaponType::EWT_Shotgun;

	if (bJumpToShotgunEnd)
	{
		JumpToShotGunEnd();
	}
}

void UCombatComponent::OnRep_Grenades()
{
	UpdateHUDGrenades();
}

void UCombatComponent::InitializeCarriedAmmo()
{
	CarriedAmmoMap.Emplace(EWeaponType::EWT_AssaultRifle, StartingARAmmo);
	CarriedAmmoMap.Emplace(EWeaponType::EWT_RocketLauncher, StartingRocketAmmo);
	CarriedAmmoMap.Emplace(EWeaponType::EWT_Pistol, StartingPistolAmmo);
	CarriedAmmoMap.Emplace(EWeaponType::EWT_SubmachinGun, StartingSMGAmmo);
	CarriedAmmoMap.Emplace(EWeaponType::EWT_Shotgun, StartingShotgunAmmo);
	CarriedAmmoMap.Emplace(EWeaponType::EWT_SniperRifle, SniperAmmo);
	CarriedAmmoMap.Emplace(EWeaponType::EWT_GrenadeLauncher, StartingGrenadeLauncherAmmo);
}

void UCombatComponent::PickupAmmo(EWeaponType WeaponType, int32 AmountAmmo)
{
	if (CarriedAmmoMap.Contains(WeaponType))
	{
		CarriedAmmoMap[WeaponType] += FMath::Clamp(CarriedAmmoMap[WeaponType] + AmountAmmo, 0, MaxCarriedAmmo);

		UpdateCarriedAmmo();
	}

	if (EquippedWeapon && EquippedWeapon->IsEmpty() && EquippedWeapon->GetWeaponType() == WeaponType)
	{
		Reload();
	}
}