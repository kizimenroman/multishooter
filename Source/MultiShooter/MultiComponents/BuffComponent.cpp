// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MultiShooter/Character/MultiCharacter.h"

UBuffComponent::UBuffComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UBuffComponent::BeginPlay()
{
	Super::BeginPlay();	
}

void UBuffComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	HealRampUp(DeltaTime);
	ShieldRampUp(DeltaTime);
}

void UBuffComponent::Heal(float HealAmount, float HealTime)
{
	bHealing = true;
	HealingRate = HealAmount / HealTime;
	AmountToHeal += HealAmount;
}

void UBuffComponent::HealRampUp(float DeltaTime)
{
	if(!bHealing || !Character || Character->IsElimed()) return;

		const float HealThisFrame = HealingRate * DeltaTime;
		Character->SetCurrentHealth(
			FMath::Clamp(
				Character->GetCurrentHealth() + HealThisFrame,
				0.f,
				Character->GetMaxHealth())
		);

		Character->UpdateHUDHealth();
		AmountToHeal -= HealThisFrame;

		if (AmountToHeal <= 0.f || Character->GetCurrentHealth() >= Character->GetMaxHealth())
		{
			bHealing = false;
			AmountToHeal = 0.f;
		}
}

void UBuffComponent::ReplenishShield(float AmountReplenish, float TimeReplenish)
{
	bReplenishing = true;
	ReplenishRate = AmountReplenish / TimeReplenish;
	AmountToReplenish += AmountReplenish;
}

void UBuffComponent::ShieldRampUp(float DeltaTime)
{
	if(!bReplenishing || !Character || Character->IsElimed()) return;

		const float ReplanishThisFrame = ReplenishRate * DeltaTime;
		Character->SetCurrentShield(
			FMath::Clamp(
				Character->GetCurrentShield() + ReplanishThisFrame,
				0.f,
				Character->GetMaxShield())
		);

		Character->UpdateHUDShield();
		AmountToReplenish -= ReplanishThisFrame;

		if (AmountToReplenish <= 0.f || Character->GetCurrentShield() >= Character->GetMaxShield())
		{
			bReplenishing = false;
			AmountToReplenish = 0.f;
		}
}

void UBuffComponent::SetInitialSpeeds(float DefaultStandSpeedToSet, float DefaultCrouchSpeedToSet)
{
	DefaultStandSpeed = DefaultStandSpeedToSet;
	DefaultCrouchSpeed = DefaultCrouchSpeedToSet;
}

void UBuffComponent::BuffSpeed(float BaseBuff, float CrouchBuff, float TimeBuff)
{
	if(!Character) return;
		
		Character->GetWorldTimerManager().SetTimer(
			SpeedBuffTimer,
			this,
			&UBuffComponent::ResetSpeeds,
			TimeBuff
		);

		if (Character->GetCharacterMovement())
		{
			Character->GetCharacterMovement()->MaxWalkSpeed = BaseBuff;
			Character->GetCharacterMovement()->MaxWalkSpeedCrouched = CrouchBuff;
		}
		MultiCastSpeedBuff(BaseBuff, CrouchBuff);
}

void UBuffComponent::ResetSpeeds()
{
	if(!Character || !Character->GetCharacterMovement()) return;

		Character->GetCharacterMovement()->MaxWalkSpeed = DefaultStandSpeed;
		Character->GetCharacterMovement()->MaxWalkSpeedCrouched = DefaultCrouchSpeed;
		MultiCastSpeedBuff(DefaultStandSpeed, DefaultCrouchSpeed);
}

void UBuffComponent::MultiCastSpeedBuff_Implementation(float StandBaseSpeed, float CrouchBaseSpeed)
{
	if(!Character || !Character->GetCharacterMovement()) return;

		Character->GetCharacterMovement()->MaxWalkSpeed = StandBaseSpeed;
		Character->GetCharacterMovement()->MaxWalkSpeedCrouched = CrouchBaseSpeed;
}

void UBuffComponent::SetDefaultJumpVelocity(float DefaultVelocity)
{
	DefaultJumpVelocity = DefaultVelocity;
}

void UBuffComponent::BuffJump(float BuffJumpVelocity, float BuffTimeVelocity)
{
	if(!Character) return;
		
		Character->GetWorldTimerManager().SetTimer(
			JumpBuffTimer,
			this,
			&UBuffComponent::ResetJump,
			BuffTimeVelocity
		);

		if (Character->GetCharacterMovement())
		{
			Character->GetCharacterMovement()->JumpZVelocity = BuffJumpVelocity;
		}
		MulticastJumpBuff(BuffJumpVelocity);
}

void UBuffComponent::ResetJump()
{
	if (Character && Character->GetCharacterMovement())
	{
		Character->GetCharacterMovement()->JumpZVelocity = DefaultJumpVelocity;
	}

	MulticastJumpBuff(DefaultJumpVelocity);
}

void UBuffComponent::MulticastJumpBuff_Implementation(float JumpVelocity)
{
	if (Character && Character->GetCharacterMovement())
	{
		Character->GetCharacterMovement()->JumpZVelocity = JumpVelocity;
	}
}