// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MultiShooter/HUD/MultiHUD.h"
#include "MultiShooter/MultiTypes/CombatState.h"
#include "MultiShooter/Weapon/WeaponTypes.h"
#include "CombatComponent.generated.h"


class ABaseWeapon;
class MultiCharacter;
class AMultiPlayerController;
class AMultiHUD;
class AProjectile;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MULTISHOOTER_API UCombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UCombatComponent();
	friend class AMultiCharacter;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void DoEquipWeapon(ABaseWeapon* WeaponToEquip);
	void SwapWeapons();
	void Reload();

	UFUNCTION(BlueprintCallable)
	void FinishReloading();

	UFUNCTION(BlueprintCallable)
	void ShotgunShellReload();

	void JumpToShotGunEnd();

	UFUNCTION(BlueprintCallable)
	void ThrowGrenadeFinished();

	UFUNCTION(BlueprintCallable)
	void LaunchGrenade();

	UFUNCTION(Server, Reliable)
	void ServerLaunchGrenade(const FVector_NetQuantize& Target);

	void PickupAmmo(EWeaponType WeaponType, int32 AmountAmmo);

	bool bLocallyReloading = false;

protected:
	///////////////////////////////////////////////////
	/*
	* Aiming
	*/
	void SetAiming(bool bIsAiming);

	UFUNCTION(Server, Reliable)
	void ServerSetAiming(bool bIsAiming);
	///////////////////////////////////////////////////
	/*
	* Fire
	*/
	void FireButtonPressed(bool bPressed);
	void Fire();
	void FireProjectileWeapon();
	void FireHitScanWeapon();
	void FireShotgun();
	void LocalFire(const FVector_NetQuantize& TraceHitTarget);
	void ShotgunLocalFire(const TArray<FVector_NetQuantize>& TraceHitTargets);

	UFUNCTION(Server, Reliable)
	void ServerFire(const FVector_NetQuantize& TraceHitTarget);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastFire(const FVector_NetQuantize& TraceHitTarget);

	UFUNCTION(Server, Reliable)
	void ServerShotgunFire(const TArray<FVector_NetQuantize>& TraceHitTargets);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastShotgunFire(const TArray<FVector_NetQuantize>& TraceHitTargets);
	///////////////////////////////////////////////////
	/*
	* Reload
	*/
	void HandleReload();

	UFUNCTION(Server, Reliable)
	void ServerReload();

	int32 AmountToReload();
	///////////////////////////////////////////////////
	/*
	* Grenade
	*/
	void ThrowGrenade();
	void ShowAttachedGrenade(bool bShow);

	UFUNCTION(Server, Reliable)
	void ServerThrowGrenade();

	UPROPERTY(EditAnywhere)
	TSubclassOf<AProjectile> GrenadeClass;	
	///////////////////////////////////////////////////
	/*
	* Other
	*/
	virtual void BeginPlay() override;
	void TraceUnderCrosshairs(FHitResult& TraceHitResult);
	void SetHUDCrosshairs(float DeltaTime);
	void DropEquipWeapon();
	void AttachActorToRightHand(AActor* ActorToAttach);
	void AttachActorToLeftHand(AActor* ActorToAttach);
	void AttachActorToBackPack(AActor* ActorToAttach);
	void UpdateCarriedAmmo();
	void PlayEquipWeaponSound(ABaseWeapon* Weapon);
	void AutoReloadCheck();
	void EquipPrimaryWeapon(ABaseWeapon* WeaponToEquip);
	void EquipSecondWeapon(ABaseWeapon* WeaponToEquip);

private:
	///////////////////////////////////////
	/*
	*  Setup 
	*/
	UPROPERTY()
	AMultiCharacter* Character;

	UPROPERTY()
	AMultiPlayerController* MultiController;

	UPROPERTY()
	AMultiHUD* HUD;	

	UPROPERTY(ReplicatedUsing = OnRep_EquippedWeapon)
	ABaseWeapon* EquippedWeapon;

	UPROPERTY(ReplicatedUsing = OnRep_SecondWeapon)
	ABaseWeapon* SecondWeapon;

	UFUNCTION()
	void OnRep_EquippedWeapon();

	UFUNCTION()
	void OnRep_SecondWeapon();

	UPROPERTY(ReplicatedUsing = OnRep_CombatState)
	ECombatState CombatState = ECombatState::ECS_Unoccupied;

	UFUNCTION()
	void OnRep_CombatState();

	UPROPERTY(EditAnywhere)
	float BaseWalkSpeed;

	UPROPERTY(EditAnywhere)
	float AimWalkSpeed;

	FHUDPackage HUDPackage;
	///////////////////////////////////////
	/*
	*  Aiming and FOV  
	*/
	UPROPERTY(EditAnywhere, Category = "Combat")
	float ZoomedFOV = 30.f;

	UPROPERTY(EditAnywhere, Category = "Combat")
	float ZoomInterpSpeed = 20.f;

	UPROPERTY(ReplicatedUsing = OnRep_Aiming)
	bool IsAiming = false;

	bool bAimButtonPressed = false;

	UFUNCTION()
	void OnRep_Aiming();

	float DefaultFOV;
	float CurrentFOV;

	void InterpFOV(float DeltaTime);
	///////////////////////////////////////
	/*
	* Crosshair set
	*/
	float CrosshairVelocityFactor;
	float CrosshairInAirFactor;
	float CrosshairAimFactor;
	float CrosshairShootingFactor;
	//////////////////////////////////////
	/*
	* Fire 
	*/
	bool bFireButtonPressed;
	bool bCanFire = true;

	FTimerHandle FireTimer;
	FVector HitTarget;

	void StartFireTimer();
	void FinishedFireTimer();
	bool CanFire();
	//////////////////////////////////////
	/*
	* Ammo\Reload system
	*/
	// Carried ammo for the currently-equipped weapon
	UPROPERTY(ReplicatedUsing = OnRep_CarriedAmmo)
	int32 CarriedAmmo;

	UPROPERTY(EditAnywhere)
	int32 StartingARAmmo = 30;

	UPROPERTY(EditAnywhere)
	int32 StartingRocketAmmo = 0;

	UPROPERTY(EditAnywhere)
	int32 StartingPistolAmmo = 0;

	UPROPERTY(EditAnywhere)
	int32 StartingSMGAmmo = 0;

	UPROPERTY(EditAnywhere)
	int32 StartingShotgunAmmo = 0;

	UPROPERTY(EditAnywhere)
	int32 SniperAmmo = 0;

	UPROPERTY(EditAnywhere)
	int32 StartingGrenadeLauncherAmmo = 0;

	UPROPERTY(ReplicatedUsing = OnRep_Grenades)
	int32 Grenades = 4;

	UFUNCTION()
	void OnRep_Grenades();

	UPROPERTY(EditAnywhere)
	int32 MaxGrenades = 4;

	void UpdateHUDGrenades();

	TMap<EWeaponType, int32> CarriedAmmoMap;

	UPROPERTY(EditAnywhere)
	int32 MaxCarriedAmmo = 128;

	UFUNCTION()
	void OnRep_CarriedAmmo();

	void InitializeCarriedAmmo();
	void UpdateAmmoValues();
	void UpdateShotgunAmmoValues();

public:	
	FORCEINLINE int32 GetGrenades() const { return Grenades; }
	bool ShouldSwapWeapons();
};
