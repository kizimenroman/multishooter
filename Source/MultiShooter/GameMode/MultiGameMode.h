// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "MultiGameMode.generated.h"


namespace MatchState
{
	extern MULTISHOOTER_API const FName Cooldown;
}


class AMultiCharacter;
class AMultiPlayerController;

UCLASS()
class MULTISHOOTER_API AMultiGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	AMultiGameMode();

	UPROPERTY(EditDefaultsOnly)
	float WarmupTime = 10.f;

	UPROPERTY(EditDefaultsOnly)
	float MatchTime = 120.f;

	UPROPERTY(EditDefaultsOnly)
	float CooldownTime = 10.f;

	float LevelStartingTime = 0.f;

	virtual void Tick(float DeltaTime) override;
	virtual void PlayerEliminated(AMultiCharacter* EllimedCharacter, AMultiPlayerController* VictimController, AMultiPlayerController* AttackerController);
	virtual void RequestRespawn(ACharacter* ElimmedCharacter, AController* ElimmedController);

protected:
	virtual void BeginPlay() override;
	virtual void OnMatchStateSet() override;

private:
	float CountdownTime = 0.f;

public:
	FORCEINLINE float GetCountdownTime() const {return CountdownTime;}
};
