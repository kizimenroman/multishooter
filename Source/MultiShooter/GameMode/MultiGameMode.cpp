// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiGameMode.h"
#include "../Character/MultiCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerStart.h"
#include "MultiShooter/PlayerState/MultiPlayerState.h"
#include "MultiShooter/GameState/MultiGameState.h"
#include "../PlayerController/MultiPlayerController.h"

namespace MatchState
{
	const FName Cooldown = FName("Cooldown");
}

AMultiGameMode::AMultiGameMode()
{
	bDelayedStart = true;
}

void AMultiGameMode::BeginPlay()
{
	Super::BeginPlay();

	LevelStartingTime = GetWorld()->GetTimeSeconds();
}

void AMultiGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (MatchState == MatchState::WaitingToStart)
	{
		CountdownTime = WarmupTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			StartMatch();
		}
	}
	else if (MatchState == MatchState::InProgress)
	{
		CountdownTime = WarmupTime + MatchTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			SetMatchState(MatchState::Cooldown);
		}
	}
	else if (MatchState == MatchState::Cooldown)
	{
		CountdownTime = CooldownTime + WarmupTime + MatchTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			RestartGame();
			//SetMatchState(MatchState::WaitingToStart);
		}
	}
}

void AMultiGameMode::OnMatchStateSet()
{
	Super::OnMatchStateSet();

	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		AMultiPlayerController* MultiPlayer = Cast<AMultiPlayerController>(*It);
		if (MultiPlayer)
		{
			MultiPlayer->OnMatchStateSet(MatchState);
		}
	}
}

void AMultiGameMode::PlayerEliminated(AMultiCharacter* EllimedCharacter, AMultiPlayerController* VictimController, AMultiPlayerController* AttackerController)
{
	AMultiPlayerState* AttackerState = AttackerController ?  Cast<AMultiPlayerState>(AttackerController->PlayerState) : nullptr;
	AMultiPlayerState* VictimState = VictimController ? Cast<AMultiPlayerState>(VictimController->PlayerState) : nullptr;
	AMultiGameState* MultiState = GetGameState<AMultiGameState>();

	if (AttackerState && AttackerState != VictimState && MultiState)
	{
		AttackerState->AddToScore(1.f);
		MultiState->UpdateTopScore(AttackerState);
	}

	if (VictimState)
	{
		VictimState->AddToDefeats(1);
	}

	if (EllimedCharacter)
	{
		EllimedCharacter->Elim();
	}
}

void AMultiGameMode::RequestRespawn(ACharacter* ElimmedCharacter, AController* ElimmedController)
{
	if (ElimmedCharacter)
	{
		ElimmedCharacter->Reset();
		ElimmedCharacter->Destroy();
	}
	if (ElimmedController)
	{
		TArray<AActor*> PlayerStarts;
		UGameplayStatics::GetAllActorsOfClass(this, APlayerStart::StaticClass(), PlayerStarts);
		int32 Selection = FMath::RandRange(0, PlayerStarts.Num() -1);

		RestartPlayerAtPlayerStart(ElimmedController, PlayerStarts[Selection]);
	}
}