// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InterractWithCrosshairsInterface.generated.h"

UINTERFACE(MinimalAPI)
class UInterractWithCrosshairsInterface : public UInterface
{
	GENERATED_BODY()
};

class MULTISHOOTER_API IInterractWithCrosshairsInterface
{
	GENERATED_BODY()

public:
};
