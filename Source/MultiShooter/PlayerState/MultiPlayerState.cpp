// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiPlayerState.h"
#include "MultiShooter/Character/MultiCharacter.h"
#include "Net/UnrealNetwork.h"
#include "MultiShooter/PlayerController/MultiPlayerController.h"


void AMultiPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMultiPlayerState, Defeats);
}

void AMultiPlayerState::AddToScore(float ScoreAmount)
{
	SetScore(GetScore() + ScoreAmount);

	MultiCharacter = MultiCharacter == nullptr ? Cast<AMultiCharacter>(GetPawn()) : MultiCharacter;
	if (MultiCharacter)
	{
		MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(MultiCharacter->Controller) : MultiController;
		if (MultiController)
		{
			MultiController->SetHUDScore(GetScore());
		}
	}
}

void AMultiPlayerState::OnRep_Score()
{
	Super::OnRep_Score();

	MultiCharacter = MultiCharacter == nullptr ? Cast<AMultiCharacter>(GetPawn()) : MultiCharacter;
	if (MultiCharacter)
	{
		MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(MultiCharacter->Controller) : MultiController;
		if (MultiController)
		{
			MultiController->SetHUDScore(GetScore());
		}
	}
}

void AMultiPlayerState::AddToDefeats(int32 DefeatsAmount)
{
	Defeats += DefeatsAmount;

	MultiCharacter = MultiCharacter == nullptr ? Cast<AMultiCharacter>(GetPawn()) : MultiCharacter;
	if (MultiCharacter)
	{
		MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(MultiCharacter->Controller) : MultiController;
		if (MultiController)
		{
			MultiController->SetHUDDefeats(Defeats);
		}
	}
}

void AMultiPlayerState::OnRep_Defeats()
{
		MultiCharacter = MultiCharacter == nullptr ? Cast<AMultiCharacter>(GetPawn()) : MultiCharacter;
	if (MultiCharacter)
	{
		MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(MultiCharacter->Controller) : MultiController;
		if (MultiController)
		{
			MultiController->SetHUDDefeats(Defeats);
		}
	}
}