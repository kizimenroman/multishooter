// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiAnimInstance.h"
#include "MultiCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "MultiShooter/MultiTypes/CombatState.h"
#include "MultiShooter/Weapon/BaseWeapon.h"
#include "GameFramework/CharacterMovementComponent.h"

void UMultiAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	MultiCharacter = Cast<AMultiCharacter>(TryGetPawnOwner());
}

void UMultiAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (!MultiCharacter)
	{
		MultiCharacter = Cast<AMultiCharacter>(TryGetPawnOwner());
	}

	if(!MultiCharacter) return;

		// Set variables
		FVector Velocity = MultiCharacter->GetVelocity();
		Velocity.Z = 0.0f;
		Speed = Velocity.Size();
		bIsInAir = MultiCharacter->GetCharacterMovement()->IsFalling();
		bIsAccelerating = MultiCharacter->GetCharacterMovement()->GetCurrentAcceleration().Size() > 0.0f ? true : false;
		bWeaponEquipped = MultiCharacter->IsWeaponEquipped();
		EquippedWeapon = MultiCharacter->GetEquippedWeapon();
		bIsCrouched = MultiCharacter->bIsCrouched;
		bAiming = MultiCharacter->IsAiming();
		bRotateRootBone = MultiCharacter->ShouldRotateRootBone();
		TIP = MultiCharacter->GetTurningState();
		bElimed = MultiCharacter->IsElimed();

		//Offset Yaw For strafing while equipped
		FRotator PlayerViewRotation = MultiCharacter->GetBaseAimRotation();
		FRotator CharacterPawnRotation = UKismetMathLibrary::MakeRotFromX(MultiCharacter->GetVelocity());
		FRotator DeltaRot = UKismetMathLibrary::NormalizedDeltaRotator(CharacterPawnRotation, PlayerViewRotation);
		DeltaRotation = FMath::RInterpTo(DeltaRotation, DeltaRot, DeltaSeconds, 6.0f);
		YawOffset = DeltaRotation.Yaw;

		CharacterRotationLastFrame = CharacterRotation;
		CharacterRotation = MultiCharacter->GetActorRotation();
		const FRotator DeltaLean = UKismetMathLibrary::NormalizedDeltaRotator(CharacterRotation, CharacterRotationLastFrame);
		const float Target = DeltaLean.Yaw / DeltaSeconds;
		const float Interp = FMath::FInterpTo(Lean, Target, DeltaSeconds, 6.f);
		Lean = FMath::Clamp(Interp, -90.f, 90.f);

		AO_Yaw = MultiCharacter->GetAO_Yaw();
		AO_Pitch = MultiCharacter->GetAO_Pitch();

		if (bWeaponEquipped && EquippedWeapon && EquippedWeapon->GetWeaponMesh() && MultiCharacter->GetMesh())
		{
			LeftHandTransform = EquippedWeapon->GetWeaponMesh()->GetSocketTransform(FName("LeftHandSocket"), ERelativeTransformSpace::RTS_World);
			FVector OutPosition;
			FRotator OutRotation;
			MultiCharacter->GetMesh()->TransformToBoneSpace(FName("Hand_R"), LeftHandTransform.GetLocation(), FRotator::ZeroRotator, OutPosition, OutRotation);
			LeftHandTransform.SetLocation(OutPosition);
			LeftHandTransform.SetRotation(FQuat(OutRotation));

			if(MultiCharacter->IsLocallyControlled())
			{
				bLocallyControlled = true;
				FTransform RightHandTransform = MultiCharacter->GetMesh()->GetSocketTransform(FName("Hand_R"), ERelativeTransformSpace::RTS_World);
				FRotator LookAt = UKismetMathLibrary::FindLookAtRotation(RightHandTransform.GetLocation(), RightHandTransform.GetLocation() + (RightHandTransform.GetLocation() - MultiCharacter->GetHitTarget()));
				RightHandRotation = FMath::RInterpTo(RightHandTransform.GetRotation().Rotator(), LookAt, DeltaSeconds, 10.f);
			}
		}



		bUseFABRIK = MultiCharacter->GetCombatState() == ECombatState::ECS_Unoccupied;
		if (MultiCharacter->IsLocallyControlled() && MultiCharacter->GetCombatState() != ECombatState::ECS_ThrowingGrenade)
		{
			bUseFABRIK = !MultiCharacter->IsLocallyReloading();
		}

		bUseAimOffsets = MultiCharacter->GetCombatState() == ECombatState::ECS_Unoccupied && !MultiCharacter->GetDisableGameplay();
		bTransformRightHand = MultiCharacter->GetCombatState() == ECombatState::ECS_Unoccupied && !MultiCharacter->GetDisableGameplay();
}
