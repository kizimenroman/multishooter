// Fill out your copyright notice in the Description page of Project Settings.

#include "MultiCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/WidgetComponent.h"
#include "Net/UnrealNetwork.h"
#include "MultiShooter/Weapon/BaseWeapon.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MultiAnimInstance.h"
#include "MultiShooter/MultiShooter.h"
#include "MultiShooter/PlayerController/MultiPlayerController.h"
#include "Camera/CameraComponent.h"
#include "MultiShooter/PlayerState/MultiPlayerState.h"
#include "Sound/SoundCue.h"
#include "TimerManager.h"
#include "Particles/ParticleSystemComponent.h"
#include "../GameMode/MultiGameMode.h"
#include "MultiShooter/Weapon/WeaponTypes.h"
#include "MultiShooter/MultiComponents/CombatComponent.h"
#include "MultiShooter/MultiComponents/BuffComponent.h"
#include "Components/BoxComponent.h"
#include "MultiShooter/MultiComponents/LagCompensationComponent.h"

AMultiCharacter::AMultiCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	NetUpdateFrequency = 66.f;
	MinNetUpdateFrequency = 33.f;
	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	Spring = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	Spring->SetupAttachment(GetMesh());
	Spring->TargetArmLength = 600.0f;
	Spring->bUsePawnControlRotation = true;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(Spring, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false;

	OverHeadWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("OverHeadWidget"));
	OverHeadWidget->SetupAttachment(RootComponent);

	Combat = CreateDefaultSubobject<UCombatComponent>(TEXT("CombapComp"));
	Combat->SetIsReplicated(true);

	Buff = CreateDefaultSubobject<UBuffComponent>(TEXT("Buff"));
	Buff->SetIsReplicated(true);

	LagCompensation = CreateDefaultSubobject<ULagCompensationComponent>(FName("LagCompensation"));

	DissolveTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("Dessolve Timeline"));

	AttachedGrenade = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Attach Grenade"));
	AttachedGrenade->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	AttachedGrenade->SetupAttachment(GetMesh(), FName("GrenadeSocket"));

	TurningInPlace = ETurningInPlace::ETIP_NotTurning;
	bUseControllerRotationYaw = false;
	GetCharacterMovement()->bOrientRotationToMovement = true;

	GetCharacterMovement()->NavAgentProps.bCanCrouch = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 720.f, 0.f);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionObjectType(ECC_SkeletalMesh);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);

	GetMesh()->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;

	/*
	* Hit boxes for server-side rewind
	*/
	head = CreateDefaultSubobject<UBoxComponent>(TEXT("head"));
	head->SetupAttachment(GetMesh(), FName("head"));
	head->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("head"), head);

	pelvis = CreateDefaultSubobject<UBoxComponent>(TEXT("pelvis"));
	pelvis->SetupAttachment(GetMesh(), FName("pelvis"));
	pelvis->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("pelvis"), pelvis);

	spine_02 = CreateDefaultSubobject<UBoxComponent>(TEXT("spine_02"));
	spine_02->SetupAttachment(GetMesh(), FName("spine_02"));
	spine_02->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("spine_02"), spine_02);

	spine_03 = CreateDefaultSubobject<UBoxComponent>(TEXT("spine_03"));
	spine_03->SetupAttachment(GetMesh(), FName("spine_03"));
	spine_03->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("spine_03"), spine_03);

	upperarm_l = CreateDefaultSubobject<UBoxComponent>(TEXT("upperarm_l"));
	upperarm_l->SetupAttachment(GetMesh(), FName("upperarm_l"));
	upperarm_l->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("upperarm_l"), upperarm_l);

	upperarm_r = CreateDefaultSubobject<UBoxComponent>(TEXT("upperarm_r"));
	upperarm_r->SetupAttachment(GetMesh(), FName("upperarm_r"));
	upperarm_r->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("upperarm_r"), upperarm_r);

	lowerarm_l = CreateDefaultSubobject<UBoxComponent>(TEXT("lowerarm_l"));
	lowerarm_l->SetupAttachment(GetMesh(), FName("lowerarm_l"));
	lowerarm_l->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("lowerarm_l"), lowerarm_l);

	lowerarm_r = CreateDefaultSubobject<UBoxComponent>(TEXT("lowerarm_r"));
	lowerarm_r->SetupAttachment(GetMesh(), FName("lowerarm_r"));
	lowerarm_r->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("lowerarm_r"), lowerarm_r);

	hand_l = CreateDefaultSubobject<UBoxComponent>(TEXT("hand_l"));
	hand_l->SetupAttachment(GetMesh(), FName("hand_l"));
	hand_l->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("hand_l"), hand_l);

	hand_r = CreateDefaultSubobject<UBoxComponent>(TEXT("hand_r"));
	hand_r->SetupAttachment(GetMesh(), FName("hand_r"));
	hand_r->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("hand_r"), hand_r);

	backpack = CreateDefaultSubobject<UBoxComponent>(TEXT("backpack"));
	backpack->SetupAttachment(GetMesh(), FName("backpack"));
	backpack->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("backpack"), backpack);

	blanket = CreateDefaultSubobject<UBoxComponent>(TEXT("blanket"));
	blanket->SetupAttachment(GetMesh(), FName("backpack"));
	blanket->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("blanket"), blanket);

	thigh_l = CreateDefaultSubobject<UBoxComponent>(TEXT("thigh_l"));
	thigh_l->SetupAttachment(GetMesh(), FName("thigh_l"));
	thigh_l->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("thigh_l"), thigh_l);

	thigh_r = CreateDefaultSubobject<UBoxComponent>(TEXT("thigh_r"));
	thigh_r->SetupAttachment(GetMesh(), FName("thigh_r"));
	thigh_r->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("thigh_r"), thigh_r);

	calf_l = CreateDefaultSubobject<UBoxComponent>(TEXT("calf_l"));
	calf_l->SetupAttachment(GetMesh(), FName("calf_l"));
	calf_l->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("calf_l"), calf_l);

	calf_r = CreateDefaultSubobject<UBoxComponent>(TEXT("calf_r"));
	calf_r->SetupAttachment(GetMesh(), FName("calf_r"));
	calf_r->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("calf_r"), calf_r);

	foot_l = CreateDefaultSubobject<UBoxComponent>(TEXT("foot_l"));
	foot_l->SetupAttachment(GetMesh(), FName("foot_l"));
	foot_l->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("foot_l"), foot_l);

	foot_r = CreateDefaultSubobject<UBoxComponent>(TEXT("foot_r"));
	foot_r->SetupAttachment(GetMesh(), FName("foot_r"));
	foot_r->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitCollisionBoxes.Emplace(FName("foot_r"), foot_r);
}

void AMultiCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AMultiCharacter, OverlappingWeapon, COND_OwnerOnly);
	DOREPLIFETIME(AMultiCharacter, CurrentHealth);
	DOREPLIFETIME(AMultiCharacter, bDisableGameplay);
	DOREPLIFETIME(AMultiCharacter, CurrentShield);
}

void AMultiCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (Combat)
	{
		Combat->Character = this;
	}

	if (Buff)
	{
		Buff->Character = this;

		if (GetCharacterMovement())
		{
			Buff->SetInitialSpeeds(
				GetCharacterMovement()->MaxWalkSpeed, 
				GetCharacterMovement()->MaxWalkSpeedCrouched
			);

			Buff->SetDefaultJumpVelocity(GetCharacterMovement()->JumpZVelocity);
		}
	}

	if (LagCompensation)
	{
		LagCompensation->Character = this;

		if (Controller)
		{
			LagCompensation->Controller = Cast<AMultiPlayerController>(Controller);
		}
	}
}

void AMultiCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward/Backward",this, &AMultiCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight",this, &AMultiCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Turn",this, &AMultiCharacter::TurnRight);
	PlayerInputComponent->BindAxis("LookUp",this, &AMultiCharacter::LookUp);

	PlayerInputComponent->BindAction("Jump",IE_Pressed, this, &AMultiCharacter::Jump);
	PlayerInputComponent->BindAction("Equip", IE_Pressed, this, &AMultiCharacter::EquipedButtonPressed);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AMultiCharacter::CrouchButtonPressed);
	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &AMultiCharacter::AimButtonPressed);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &AMultiCharacter::AimButtonReleased);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AMultiCharacter::FireButtonPressed);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AMultiCharacter::FireButtonReleased);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AMultiCharacter::ReloadButtonPressed);
	PlayerInputComponent->BindAction("ThrowGrenade", IE_Pressed, this, &AMultiCharacter::GrenadeButtonPressed);
}

void AMultiCharacter::BeginPlay()
{
	Super::BeginPlay();
		
	SpawnDefaultWeapon();
	UpdateHUDAmmo();
	UpdateHUDHealth();
	UpdateHUDShield();

	if (HasAuthority())
	{
		OnTakeAnyDamage.AddDynamic(this, &AMultiCharacter::ReceiveDamage);
	}

	if (AttachedGrenade)
	{
		AttachedGrenade->SetVisibility(false);
	}
}

void AMultiCharacter::SpawnDefaultWeapon()
{
	AMultiGameMode* MultiGM = Cast<AMultiGameMode>(UGameplayStatics::GetGameMode(this));
	if (MultiGM && GetWorld() && !bElimed && StartWeaponClass)
	{
		ABaseWeapon* SpawnedStartWeapon = GetWorld()->SpawnActor<ABaseWeapon>(StartWeaponClass);
		if (SpawnedStartWeapon && Combat)
		{
			Combat->DoEquipWeapon(SpawnedStartWeapon);
			SpawnedStartWeapon->bDestroyedWeapon = true;
		}
	}
}

void AMultiCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	RotateInPlace(DeltaTime);

	if (IsLocallyControlled())
	{
		HideCameraIfCharacterClose();
	}

	if (GetWorld()->GetTimeSeconds() < 1.0f)
	{
		PollInit();
	}
}

void AMultiCharacter::RotateInPlace(float DeltaTime)
{
	if (bDisableGameplay)
	{
		bUseControllerRotationYaw = false;
		TurningInPlace = ETurningInPlace::ETIP_NotTurning;
		return;
	}

	if (GetLocalRole() > ENetRole::ROLE_SimulatedProxy && IsLocallyControlled())
	{
		AimOffset(DeltaTime);
	}
	else
	{
		TimeSinceLastMovementRep += DeltaTime;
		if (TimeSinceLastMovementRep > 0.25f)
		{
			OnRep_ReplicatedMovement();
		}
		CalculateAO_Pitch();
	}
}

void AMultiCharacter::SetOverlappingWeapon(ABaseWeapon* Weapon)
{
	if (OverlappingWeapon)
	{
		OverlappingWeapon->ShowPickupWidget(false);
	}

	 OverlappingWeapon = Weapon;

	if (IsLocallyControlled())
	{
		if (OverlappingWeapon)
		{
			OverlappingWeapon->ShowPickupWidget(true);
		}
	}
}

void AMultiCharacter::OnRep_OverlappingWeapon(ABaseWeapon* LastWeapon)
{
	if (OverlappingWeapon)
	{
		OverlappingWeapon->ShowPickupWidget(true);
	}
	if (LastWeapon)
	{
		LastWeapon->ShowPickupWidget(false);
	}
}

void AMultiCharacter::MoveForward(float Axis)
{
	if(bDisableGameplay) return;

	if (Controller && Axis != 0)
	{
		const FRotator YawRotation(0.0f, Controller->GetControlRotation().Yaw, 0.0f);
		const FVector Direction(FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X));
		AddMovementInput(Direction, Axis);
	}
}

void AMultiCharacter::MoveRight(float Axis)
{
	if(bDisableGameplay) return;

	if (Controller && Axis != 0)
	{
		const FRotator YawRotation(0.0f, Controller->GetControlRotation().Yaw, 0.0f);
		const FVector Direction(FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y));
		AddMovementInput(Direction, Axis);
	}
}

void AMultiCharacter::LookUp(float Axis)
{
	AddControllerPitchInput(Axis);
}

void AMultiCharacter::TurnRight(float Axis)
{
	AddControllerYawInput(Axis);
}

void AMultiCharacter::EquipedButtonPressed()
{
	if(bDisableGameplay) return;

		if (Combat)
		{
			ServerEquipButtonPressed();
		}
}

void AMultiCharacter::ServerEquipButtonPressed_Implementation()
{
	if (Combat)
	{
		if (OverlappingWeapon)
		{
			Combat->DoEquipWeapon(OverlappingWeapon);
		}
		else if (Combat->ShouldSwapWeapons())
		{
			Combat->SwapWeapons();
		}
	}
}

void AMultiCharacter::CrouchButtonPressed()
{
	if(bDisableGameplay) return;

	if(bIsCrouched)
	{
		UnCrouch();
	}
	else
	{
		Crouch();
	}
}

void AMultiCharacter::FireButtonPressed()
{
	if(bDisableGameplay) return;

	if (Combat)
	{
		Combat->FireButtonPressed(true);
	}
}

void AMultiCharacter::FireButtonReleased()
{
	if (Combat)
	{
		Combat->FireButtonPressed(false);
	}
}

void AMultiCharacter::AimButtonPressed()
{
	if(bDisableGameplay) return;

	if (Combat)
	{
		Combat->SetAiming(true);
	}
}

void AMultiCharacter::AimButtonReleased()
{
	if(bDisableGameplay) return;

	if (Combat)
	{
		Combat->SetAiming(false);
	}
}

bool AMultiCharacter::IsAiming()
{
	return (Combat && Combat->IsAiming);
}

void AMultiCharacter::PlayFireMontage(bool bAiming)
{
	if(!Combat || !Combat->EquippedWeapon) return;

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && FireWeaponMontage)
	{
		AnimInstance->Montage_Play(FireWeaponMontage);
		FName SectionName = bAiming ? FName("RifleAim") : FName("RifleHip");
		AnimInstance->Montage_JumpToSection(SectionName);
	}
}

void AMultiCharacter::PlayHitReactionMontage()
{
	if(!Combat || !Combat->EquippedWeapon) return;

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if(AnimInstance && HitReactMontage)
	{
		AnimInstance->Montage_Play(HitReactMontage);
		FName SectionName("FromFront");
		AnimInstance->Montage_JumpToSection(SectionName);
	}
}

bool AMultiCharacter::IsWeaponEquipped()
{
	return (Combat && Combat->EquippedWeapon);
}

void AMultiCharacter::AimOffset(float DeltaTime)
{
	if(Combat && !Combat->EquippedWeapon) return;

	bool bIsInAir = GetCharacterMovement()->IsFalling();	
	float Speed = CalculateSpeed();
	if (Speed == 0.0f && !bIsInAir)
	{
		bRotateRootBone = true;
		FRotator CurrentAimRotation = FRotator(0.f, GetBaseAimRotation().Yaw, 0.f);
		FRotator DeltaAimRotation = UKismetMathLibrary::NormalizedDeltaRotator(CurrentAimRotation, StartingAimRotation);
		AO_Yaw = DeltaAimRotation.Yaw;
		if (TurningInPlace == ETurningInPlace::ETIP_NotTurning)
		{
			InterpAO_Yaw = AO_Yaw;
		}
		bUseControllerRotationYaw = true;
		TurnInPlace(DeltaTime);
	}
	if (Speed > 0.0f || bIsInAir)
	{
		bRotateRootBone = false;
		StartingAimRotation = FRotator(0.0f, GetBaseAimRotation().Yaw, 0.0f);
		AO_Yaw = 0.f;
		bUseControllerRotationYaw = true;
		TurningInPlace = ETurningInPlace::ETIP_NotTurning;
	}

	CalculateAO_Pitch();
}

void AMultiCharacter::CalculateAO_Pitch()
{
	AO_Pitch = GetBaseAimRotation().Pitch;

	if (AO_Pitch > 90.f && !IsLocallyControlled())
	{
		// fix pitch from [270, 360) to [-90, 0)
		FVector2D InRange(270.f, 360.f);
		FVector2D OutRange(-90.f, 0.f);

		AO_Pitch = FMath::GetMappedRangeValueClamped(InRange, OutRange, AO_Pitch);
	}
}

void AMultiCharacter::SimProxiesTurn()
{
	if(!Combat || !Combat->EquippedWeapon) return;

	bRotateRootBone = false;

	float Speed = CalculateSpeed();
	if (Speed > 0.f)
	{
		TurningInPlace = ETurningInPlace::ETIP_NotTurning;
		return;
	}

	ProxyRotationLastFrame = ProxyRotation;
	ProxyRotation = GetActorRotation();
	ProxyYaw = UKismetMathLibrary::NormalizedDeltaRotator(ProxyRotation, ProxyRotationLastFrame).Yaw;

	if (FMath::Abs(ProxyYaw) > TurnThreshold)
	{
		if (ProxyYaw > TurnThreshold)
		{
			TurningInPlace = ETurningInPlace::ETIP_Right;
		}
		else if (ProxyYaw < -TurnThreshold)
		{
			TurningInPlace = ETurningInPlace::ETIP_Left;
		}
		else
		{
			TurningInPlace = ETurningInPlace::ETIP_NotTurning;
		}
		return;
	}

	TurningInPlace = ETurningInPlace::ETIP_NotTurning;
}

void AMultiCharacter::OnRep_ReplicatedMovement()
{
	Super::OnRep_ReplicatedMovement();

	SimProxiesTurn();	
	TimeSinceLastMovementRep = 0.f;
}

void AMultiCharacter::TurnInPlace(float DeltaTime)
{
	if (AO_Yaw > 90.f)
	{
		TurningInPlace = ETurningInPlace::ETIP_Right;
	}
	else if (AO_Yaw < -90.f)
	{
		TurningInPlace = ETurningInPlace::ETIP_Left;
	}
	if (TurningInPlace != ETurningInPlace::ETIP_NotTurning)
	{
		InterpAO_Yaw = FMath::FInterpTo(InterpAO_Yaw, 0.f, DeltaTime, 4.f);
		AO_Yaw = InterpAO_Yaw;

		if (FMath::Abs(AO_Yaw) < 35.f)
		{
			TurningInPlace = ETurningInPlace::ETIP_NotTurning;
			StartingAimRotation = FRotator(0.f, GetBaseAimRotation().Yaw, 0.f);
		}
	}
}

float AMultiCharacter::CalculateSpeed()
{
	FVector Velocity = GetVelocity();
	Velocity.Z = 0.0f;
	return Velocity.Size();
}

void AMultiCharacter::Jump()
{
	if(bDisableGameplay) return;

	if (bIsCrouched)
	{
		UnCrouch();
	}
	Super::Jump();
}

void AMultiCharacter::HideCameraIfCharacterClose()
{
	if ((Camera->GetComponentLocation() - GetActorLocation()).Size() < CameraThreshold)
	{
		GetMesh()->SetVisibility(false);
		if (Combat && Combat->EquippedWeapon && Combat->EquippedWeapon->GetWeaponMesh())
		{
			Combat->EquippedWeapon->GetWeaponMesh()->bOwnerNoSee = true;
		}
	}
	else
	{
		GetMesh()->SetVisibility(true);
		if (Combat && Combat->EquippedWeapon && Combat->EquippedWeapon->GetWeaponMesh())
		{
			Combat->EquippedWeapon->GetWeaponMesh()->bOwnerNoSee = false;
		}
	}
}

ABaseWeapon* AMultiCharacter::GetEquippedWeapon()
{
	if(!Combat || !Combat->EquippedWeapon) return nullptr;

	return Combat->EquippedWeapon;
}

FVector AMultiCharacter::GetHitTarget() const
{
	if(!Combat) return FVector();

	return Combat->HitTarget;
}

void AMultiCharacter::ReceiveDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if(bElimed) return;
		
		float DamageToHealth = Damage;
		if (CurrentShield > 0.f)
		{
			if (CurrentShield >= Damage)
			{
				CurrentShield = FMath::Clamp(CurrentShield - Damage, 0.f, MaxShield);
				DamageToHealth = 0;
			}
			else
			{
				DamageToHealth -= CurrentShield;
				CurrentShield = 0.f;
			}
		}

		CurrentHealth = FMath::Clamp(CurrentHealth - DamageToHealth, 0.f, MaxHealth);

		UpdateHUDHealth();
		UpdateHUDShield();
		PlayHitReactionMontage();

		if (FMath::IsNearlyZero(CurrentHealth))
		{
			MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(Controller) : MultiController;
			AMultiGameMode* MultiGM = GetWorld()->GetAuthGameMode<AMultiGameMode>();
			AMultiPlayerController* AttackerController = Cast<AMultiPlayerController>(InstigatedBy);
			if (MultiGM)
			{
				MultiGM->PlayerEliminated(this, MultiController, AttackerController);

			}
		}
}

void AMultiCharacter::OnRep_Health(float LastHealth)
{
	UpdateHUDHealth();
	if (CurrentHealth < LastHealth)
	{
		PlayHitReactionMontage();
	}
}

void AMultiCharacter::UpdateHUDHealth()
{
	MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(Controller) : MultiController;
	if (MultiController)
	{
		MultiController->SetHUDHealth(CurrentHealth, MaxHealth);
	}
}

void AMultiCharacter::OnRep_Shield(float LastShield)
{
	UpdateHUDShield();
	if (CurrentShield < LastShield)
	{
		PlayHitReactionMontage();
	}
}

void AMultiCharacter::UpdateHUDShield()
{
	MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(Controller) : MultiController;
	if (MultiController)
	{
		MultiController->SetHUDShield(CurrentShield, MaxShield);
	}
}

void AMultiCharacter::UpdateHUDAmmo()
{
	MultiController = MultiController == nullptr ? Cast<AMultiPlayerController>(Controller) : MultiController;
	if (MultiController && Combat && Combat->EquippedWeapon)
	{
		MultiController->SetHUDCarriedAmmo(Combat->CarriedAmmo);
		MultiController->SetHUDWeaponAmmo(Combat->EquippedWeapon->GetAmmo());
	}
}

void AMultiCharacter::Elim()
{
	ElimWeaponsBehavior();
	MulticastElim();

	GetWorldTimerManager().SetTimer(
		ElimTimer,
		this,
		&AMultiCharacter::FinishedElimTimer,
		ElimDelay
	);
}

void AMultiCharacter::ElimWeaponsBehavior()
{
	if (Combat)
	{
		if (Combat->EquippedWeapon)
		{
			ElimWeaponBehavior(Combat->EquippedWeapon);
		}

		if (Combat->SecondWeapon)
		{
			ElimWeaponBehavior(Combat->SecondWeapon);
		}
	}
}

void AMultiCharacter::ElimWeaponBehavior(ABaseWeapon* Weapon)
{
	if (Weapon)
	{
		if (Weapon->bDestroyedWeapon)
		{
			Weapon->Destroy();
		}
		else
		{
			Weapon->Dropped();
		}
	}
}

void AMultiCharacter::MulticastElim_Implementation()
{
	if (MultiController)
	{
		MultiController->SetHUDWeaponAmmo(0);
	}

	bElimed = true;
	PlayElimMontage();

	// Start dessolve FX
	if (MI_Dissolve)
	{
		MI_DynamicDissolve = UMaterialInstanceDynamic::Create(MI_Dissolve, this);
		GetMesh()->SetMaterial(0, MI_DynamicDissolve);
		MI_DynamicDissolve->SetScalarParameterValue(TEXT("Dissolve"), 0.55f);
		MI_DynamicDissolve->SetScalarParameterValue(TEXT("Glow"), 125.f);
	}
	StartDessolveMaterial();

	// Disable Character movement
	//GetCharacterMovement()->DisableMovement();
	//GetCharacterMovement()->StopMovementImmediately();
	bDisableGameplay = true;
	GetCharacterMovement()->DisableMovement();

	// Disable collision
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	// Spawn elim bot
	if (ElimBotFx)
	{
		FVector ElimBotSpawnPoint(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + 200.f);
		ElimBotComponent = UGameplayStatics::SpawnEmitterAtLocation(
			GetWorld(),
			ElimBotFx,
			ElimBotSpawnPoint,
			FRotator::ZeroRotator
		);
	}
	if (ElimBotSound)
	{
		UGameplayStatics::PlaySoundAtLocation(
			this,
			ElimBotSound,
			GetActorLocation(),
			FRotator::ZeroRotator
		);
	}


	bool bHideSniperScope = Combat &&
		Combat->IsAiming &&
		Combat->EquippedWeapon &&
		Combat->EquippedWeapon->GetWeaponType() == EWeaponType::EWT_SniperRifle;

	if (IsLocallyControlled() && bHideSniperScope)
	{
		ShowSniperScopeWidget(false);
	}
}

void AMultiCharacter::PlayElimMontage()
{
	UAnimInstance* AnimInst =  GetMesh()->GetAnimInstance();
	if (AnimInst && ElimMontage)
	{
		AnimInst->Montage_Play(ElimMontage);
	}	
}

void AMultiCharacter::PlayThrowGrenadeMontage()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && ThrowGrenadeMontage)
	{
		AnimInstance->Montage_Play(ThrowGrenadeMontage);
	}
}

void AMultiCharacter::FinishedElimTimer()
{
	AMultiGameMode* MultiGM = GetWorld()->GetAuthGameMode<AMultiGameMode>();
	if (MultiGM)
	{
		MultiGM->RequestRespawn(this, Controller);
	}
}

void AMultiCharacter::UpdateDissolveMaterial(float DissolveValue)
{
	if (MI_DynamicDissolve)
	{
		MI_DynamicDissolve->SetScalarParameterValue(TEXT("Dissolve"), DissolveValue);
	}
}

void AMultiCharacter::StartDessolveMaterial()
{
	DissolveTrack.BindDynamic(this, &AMultiCharacter::UpdateDissolveMaterial);

	if (DissolveCurve && DissolveTimeline)
	{
		DissolveTimeline->AddInterpFloat(DissolveCurve, DissolveTrack);
		DissolveTimeline->Play();
	}
}

void AMultiCharacter::Destroyed()
{
	Super::Destroyed();

	if (ElimBotComponent)
	{
		ElimBotComponent->DestroyComponent();
	}

	AMultiGameMode* GameMode = Cast<AMultiGameMode>(UGameplayStatics::GetGameMode(this));
	bool bMatchNotInProgress = GameMode && GameMode->GetMatchState() != MatchState::InProgress;

	if (Combat && Combat->EquippedWeapon && bMatchNotInProgress)
	{
		Combat->EquippedWeapon->Destroy();
	}
}

void AMultiCharacter::PollInit()
{
	if (MultiPlayerState == nullptr)
	{
		MultiPlayerState = GetPlayerState<AMultiPlayerState>();
		if (MultiPlayerState)
		{
			MultiPlayerState->AddToScore(0.f);
			MultiPlayerState->AddToDefeats(0);
		}
	}
}

void AMultiCharacter::GrenadeButtonPressed()
{
	if (Combat)
	{
		Combat->ThrowGrenade();
	}
}

void AMultiCharacter::ReloadButtonPressed()
{
	if(bDisableGameplay) return;

	if (Combat)
	{
		Combat->Reload();
	}
}

bool AMultiCharacter::IsLocallyReloading()
{
	if (!Combat) return false;

		return Combat->bLocallyReloading;
}

void AMultiCharacter::PlayReloadMontage()
{
	if(!Combat || !Combat->EquippedWeapon) return;

	UAnimInstance* AnimInstance =  GetMesh()->GetAnimInstance();
	if (AnimInstance && ReloadMontage)
	{
		AnimInstance->Montage_Play(ReloadMontage);
		FName SectionName;

		switch (Combat->EquippedWeapon->GetWeaponType())
		{
		case EWeaponType::EWT_AssaultRifle:
			SectionName = FName(TEXT("Rifle"));
			break;
		case EWeaponType::EWT_RocketLauncher:
			SectionName = FName(TEXT("RocketLauncher"));
			break;
		case EWeaponType::EWT_Pistol:
			SectionName = FName(TEXT("Pistol"));
			break;
		case EWeaponType::EWT_SubmachinGun:
			SectionName = FName(TEXT("Pistol"));
			break;
		case EWeaponType::EWT_Shotgun:
			SectionName = FName(TEXT("Shotgun"));
			break;
		case EWeaponType::EWT_SniperRifle:
			SectionName = FName(TEXT("SniperRifle"));
			break;
		case EWeaponType::EWT_GrenadeLauncher:
			SectionName = FName(TEXT("GrenadeLauncher"));
			break;
		case EWeaponType::EWT_MAX:
			break;
		default:
			break;
		}
		AnimInstance->Montage_JumpToSection(SectionName);
	}
}

ECombatState AMultiCharacter::GetCombatState() const
{
	if(!Combat) return ECombatState::ECS_MAX;

	return Combat->CombatState;
}