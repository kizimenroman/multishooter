// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MultiShooter/MultiTypes/TurningInPlace.h"
#include "Components/TimelineComponent.h"
#include "MultiShooter/MultiTypes/CombatState.h"
#include "MultiShooter/Interfaces/InterractWithCrosshairsInterface.h"
#include "MultiCharacter.generated.h"

class USoundCue;
class UAnimMontage;
class ABaseWeapon;
class UCameraComponent;
class USpringArmComponent;
class UWidgetComponent;
class AMultiPlayerController;
class AMultiPlayerState;
class UCombatComponent;
class UBuffComponent;
class UBoxComponent;
class ULagCompensationComponent;

UCLASS()
class MULTISHOOTER_API AMultiCharacter : public ACharacter, public IInterractWithCrosshairsInterface
{
	GENERATED_BODY()

public:
	AMultiCharacter();

	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void PostInitializeComponents() override;
	virtual void OnRep_ReplicatedMovement() override;
	virtual void Destroyed() override;
	void PlayFireMontage(bool bAiming);
	void PlayReloadMontage();
	void PlayElimMontage();
	void PlayThrowGrenadeMontage();
	void Elim();
	void FireButtonReleased();
	void UpdateHUDHealth();
	void UpdateHUDShield();
	void UpdateHUDAmmo();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastElim();

	UPROPERTY(Replicated)
	bool bDisableGameplay = false;

	UFUNCTION(BlueprintImplementableEvent)
	void ShowSniperScopeWidget(bool bShowScope);

	UPROPERTY()
	TMap<FName, UBoxComponent*> HitCollisionBoxes;

protected:
	virtual void BeginPlay() override;
	void MoveForward(float Axis);
	void MoveRight(float Axis);
	void LookUp(float Axis);
	void TurnRight(float Axis);
	virtual void Jump() override;
	void CrouchButtonPressed();
	void ReloadButtonPressed();
	void EquipedButtonPressed();
	void AimButtonPressed();
	void AimButtonReleased();
	void FireButtonPressed();
	void GrenadeButtonPressed();
	
	void PlayHitReactionMontage();
	void AimOffset(float DeltaTime);
	void CalculateAO_Pitch();
	void SimProxiesTurn();
	void RotateInPlace(float DeltaTime);
	void PollInit();
	void SpawnDefaultWeapon();
	void ElimWeaponBehavior(ABaseWeapon* Weapon);
	void ElimWeaponsBehavior();

	UFUNCTION()
	void ReceiveDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser );

	/////////////////////////////////////////////////
	/*
	* Hit boxes used for server-side rewind
	*/
	UPROPERTY(EditAnywhere)
	UBoxComponent* head;

	UPROPERTY(EditAnywhere)
	UBoxComponent* pelvis;

	UPROPERTY(EditAnywhere)
	UBoxComponent* spine_02;

	UPROPERTY(EditAnywhere)
	UBoxComponent* spine_03;

	UPROPERTY(EditAnywhere)
	UBoxComponent* upperarm_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* upperarm_r;

	UPROPERTY(EditAnywhere)
	UBoxComponent* lowerarm_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* lowerarm_r;

	UPROPERTY(EditAnywhere)
	UBoxComponent* hand_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* hand_r;

	UPROPERTY(EditAnywhere)
	UBoxComponent* backpack;

	UPROPERTY(EditAnywhere)
	UBoxComponent* blanket;

	UPROPERTY(EditAnywhere)
	UBoxComponent* thigh_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* thigh_r;

	UPROPERTY(EditAnywhere)
	UBoxComponent* calf_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* calf_r;

	UPROPERTY(EditAnywhere)
	UBoxComponent* foot_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* foot_r;

private:
	/////////////////////////////////////////////////
	/*
	* Setup Character
	*/
	UPROPERTY(VisibleAnywhere, Category = "Camera")
	UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, Category = "Camera")
	USpringArmComponent* Spring;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UWidgetComponent* OverHeadWidget;

	UPROPERTY()
	AMultiPlayerController* MultiController;

	UPROPERTY()
	AMultiPlayerState* MultiPlayerState;

	UPROPERTY(EditAnywhere)
	float CameraThreshold = 200.f;

	void HideCameraIfCharacterClose();
	/////////////////////////////////////////////////
	/*
	* Set animation montages
	*/
	UPROPERTY(EditAnywhere, Category = "Combat")
	UAnimMontage* FireWeaponMontage;

	UPROPERTY(EditAnywhere, Category = "Combat")
	UAnimMontage* HitReactMontage;

	UPROPERTY(EditAnywhere, Category = "Combat")
	UAnimMontage* ReloadMontage;

	UPROPERTY(EditAnywhere, Category = "Combat")
	UAnimMontage* ElimMontage;

	UPROPERTY(EditAnywhere, Category = "Combat")
	UAnimMontage* ThrowGrenadeMontage;
	/////////////////////////////////////////////////
	/*
	* Components settings
	*/
	UPROPERTY(ReplicatedUsing = OnRep_OverlappingWeapon)
	ABaseWeapon* OverlappingWeapon;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCombatComponent* Combat;

	UFUNCTION(Server, Reliable)
	void ServerEquipButtonPressed();

	UFUNCTION()
	void OnRep_OverlappingWeapon(ABaseWeapon* LastWeapon);	

	UPROPERTY(VisibleAnywhere)
	UBuffComponent* Buff;

	UPROPERTY(VisibleAnywhere)
	ULagCompensationComponent* LagCompensation;
	/////////////////////////////////////////////////
	/*
	* Calculates for animation update
	*/
	FRotator StartingAimRotation;
	FRotator ProxyRotationLastFrame;
	FRotator ProxyRotation;
	ETurningInPlace TurningInPlace;

	bool bRotateRootBone;
	float AO_Yaw;
	float InterpAO_Yaw;
	float AO_Pitch;
	float TurnThreshold = 0.5f;
	float ProxyYaw;
	float TimeSinceLastMovementRep;

	void TurnInPlace(float DeltaTime);
	float CalculateSpeed();
	/////////////////////////////////////////////////
	/*
	* Player Health
	*/
	UPROPERTY(EditAnywhere, Category = "Player Stats")
	float MaxHealth = 100.f;

	UPROPERTY(ReplicatedUsing = OnRep_Health, VisibleAnywhere, Category = "Player Stats")
	float CurrentHealth = 100.f;

	UFUNCTION()
	void OnRep_Health(float LastHealth);
	/////////////////////////////////////////////////
	/*
	* Player Shield
	*/
	UPROPERTY(EditAnywhere, Category = "Player Stats")
	float MaxShield = 100.f;

	UPROPERTY(ReplicatedUsing = OnRep_Shield, EditAnywhere, Category = "Player Stats")
	float CurrentShield = 100.f;

	UFUNCTION()
	void OnRep_Shield(float LastShield);
	/////////////////////////////////////////////////
	/*
	* Elim character
	*/
	FTimerHandle ElimTimer;

	UPROPERTY(EditDefaultsOnly)
	float ElimDelay = 3.f;

	bool bElimed = false;

	UFUNCTION()
	void FinishedElimTimer();

	UPROPERTY(EditAnywhere)
	UParticleSystem* ElimBotFx;

	UPROPERTY(VisibleAnywhere)
	UParticleSystemComponent* ElimBotComponent;

	UPROPERTY(EditAnywhere)
	USoundCue* ElimBotSound;
	/////////////////////////////////////////////////
	/*
	* Dissolve Fx
	*/
	UPROPERTY(VisibleAnywhere)
	UTimelineComponent* DissolveTimeline; 

	UPROPERTY(EditAnywhere)
	UCurveFloat* DissolveCurve;

	// Dynamic we can change at runtime
	UPROPERTY(VisibleAnywhere, Category = "Elim")
	UMaterialInstanceDynamic* MI_DynamicDissolve;

	// Set in Blueprint and will used with DynaimcInstance
	UPROPERTY(EditAnywhere, Category = "Elim")
	UMaterialInstance* MI_Dissolve;

	FOnTimelineFloat DissolveTrack;

	UFUNCTION()
	void UpdateDissolveMaterial(float DissolveValue);
	void StartDessolveMaterial();
	////////////////////////////////////////////////////////
	/*
	* Grenade
	*/
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* AttachedGrenade;
	////////////////////////////////////////////////////////
	/*
	* Start weapon
	*/
	UPROPERTY(EditAnywhere)
	TSubclassOf<ABaseWeapon> StartWeaponClass;


public:	// Getters
	FORCEINLINE UCameraComponent* GetCameraComponent() const {return Camera;}
	FORCEINLINE ETurningInPlace GetTurningState() const {return TurningInPlace;}
	FORCEINLINE bool ShouldRotateRootBone() const {return bRotateRootBone;}
	FORCEINLINE bool IsElimed() const {return bElimed;}
	FORCEINLINE bool GetDisableGameplay() const {return bDisableGameplay;}
	FORCEINLINE float GetMaxHealth() const {return MaxHealth;}
	FORCEINLINE float GetCurrentHealth() const {return CurrentHealth;}
	FORCEINLINE float GetCurrentShield() const {return CurrentShield;}
	FORCEINLINE float GetMaxShield() const {return MaxShield;}
	FORCEINLINE float GetAO_Yaw() const {return AO_Yaw;}
	FORCEINLINE float GetAO_Pitch() const {return AO_Pitch;}
	FORCEINLINE UAnimMontage* GetReloadMontage() const {return ReloadMontage;}
	FORCEINLINE UCombatComponent* GetCombat() const {return Combat;}
	FORCEINLINE UStaticMeshComponent* GetAttachedGrenade() const {return AttachedGrenade;}
	FORCEINLINE UBuffComponent* GetBuffComponent() const {return Buff;}
	ABaseWeapon* GetEquippedWeapon();
	ECombatState GetCombatState() const;
	FVector GetHitTarget() const;
	bool IsWeaponEquipped();
	bool IsAiming();
	bool IsLocallyReloading();

	// Setters
	void SetOverlappingWeapon(ABaseWeapon* Weapon);
	FORCEINLINE void SetCurrentHealth(float Amount) {CurrentHealth = Amount;}
	FORCEINLINE void SetCurrentShield(float Amount) {CurrentShield = Amount; }
};
