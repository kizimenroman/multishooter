// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoPickup.h"
#include "MultiShooter/Character/MultiCharacter.h"
#include "MultiShooter/MultiComponents/CombatComponent.h"

void AAmmoPickup::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnSphereOverlap(OverlappedComponent,	OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	if (OtherActor)
	{
		AMultiCharacter* MultiChar = Cast<AMultiCharacter>(OtherActor);
		if (MultiChar && MultiChar->GetCombat())
		{
			MultiChar->GetCombat()->PickupAmmo(WeaponType, AmmoAmount);
		}
	}
	Destroy();
}
