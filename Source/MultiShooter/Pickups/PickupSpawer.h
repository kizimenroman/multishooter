// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickupSpawer.generated.h"

class APickup;

UCLASS()
class MULTISHOOTER_API APickupSpawer : public AActor
{
	GENERATED_BODY()
	
public:	
	APickupSpawer();
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<APickup>> PickupClasses;
	
	void SpawnPickup();
	void SpawnTimeFinished();

	UFUNCTION()
	void StartSpawnTimer(AActor* DestroyedActor);

private:	
	FTimerHandle SpawnPickupTime;

	UPROPERTY()
	APickup* Pickup;

	UPROPERTY(EditAnywhere)
	float MinSpawnTime = 3.f;

	UPROPERTY(EditAnywhere)
	float MaxSpawnTime = 10.f;
};
