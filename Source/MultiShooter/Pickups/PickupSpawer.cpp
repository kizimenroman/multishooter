// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupSpawer.h"
#include "Pickup.h"
#include "TimerManager.h"

APickupSpawer::APickupSpawer()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

}

void APickupSpawer::BeginPlay()
{
	Super::BeginPlay();
	
	StartSpawnTimer((AActor*)nullptr);
}

void APickupSpawer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickupSpawer::SpawnPickup()
{
	int32 NumPickupClasses = PickupClasses.Num();

	if (NumPickupClasses > 0 && GetWorld())
	{
		int32 Selection = FMath::RandRange(0, NumPickupClasses - 1);

		Pickup = GetWorld()->SpawnActor<APickup>(
			PickupClasses[Selection],
			GetActorTransform()			
		);

		if (HasAuthority() && Pickup)
		{
			Pickup->OnDestroyed.AddDynamic(this, &APickupSpawer::StartSpawnTimer);
		}
	}
}

void APickupSpawer::StartSpawnTimer(AActor* DestroyedActor)
{
	const float SpawnTime = FMath::FRandRange(MinSpawnTime, MaxSpawnTime);
	GetWorldTimerManager().SetTimer(
		SpawnPickupTime,
		this,
		&APickupSpawer::SpawnTimeFinished,
		SpawnTime
	);
}

void APickupSpawer::SpawnTimeFinished()
{
	if (HasAuthority())
	{
		SpawnPickup();
	}
}

