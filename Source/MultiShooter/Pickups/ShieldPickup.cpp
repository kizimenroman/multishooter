// Fill out your copyright notice in the Description page of Project Settings.


#include "ShieldPickup.h"
#include "MultiShooter/Character/MultiCharacter.h"
#include "MultiShooter/MultiComponents/BuffComponent.h"


void AShieldPickup::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnSphereOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	AMultiCharacter* MultiCharacter = Cast<AMultiCharacter>(OtherActor);
	if (MultiCharacter)
	{
		UBuffComponent* BuffComponent = MultiCharacter->GetBuffComponent();
		if (BuffComponent)
		{
			BuffComponent->ReplenishShield(ShieldAmount, ShieldTime);
		}
	}

	Destroy();
}