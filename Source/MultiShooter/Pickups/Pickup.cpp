// Fill out your copyright notice in the Description page of Project Settings.

#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Components/SphereComponent.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "MultiShooter/Weapon/WeaponTypes.h"
#include "Pickup.h"

APickup::APickup()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	InterractSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Interract Sphere"));
	InterractSphere->SetupAttachment(RootComponent);
	InterractSphere->SetSphereRadius(160.f);
	InterractSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	InterractSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	InterractSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	InterractSphere->AddLocalOffset(FVector(0.f, 0.f, 65.f));

	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body Mesh"));
	PickupMesh->SetupAttachment(RootComponent);
	PickupMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PickupMesh->SetRelativeScale3D(FVector(3.f));
	PickupMesh->SetRenderCustomDepth(true);
	PickupMesh->SetCustomDepthStencilValue(CUSTOM_DEPTH_PURPLE);

	BodyFx = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Niagara Body"));
	BodyFx->SetupAttachment(RootComponent);
}

void APickup::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		GetWorldTimerManager().SetTimer(
			DelayCollisionTimer,
			this,
			&APickup::BindOverlapTimerFinished,
			DelayCollisionSpawn
		);
	}
	
}

void APickup::BindOverlapTimerFinished()
{
	if (HasAuthority())
	{
		InterractSphere->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnSphereOverlap);
	}
}

void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (PickupMesh)
	{
		PickupMesh->AddWorldRotation(FRotator(0.f, BaseTurnRate * DeltaTime, 0.f));
	}
}

void APickup::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}


void APickup::Destroyed()
{
	Super::Destroyed();

	if (PickupSound)
	{
		UGameplayStatics::PlaySoundAtLocation(
			this,
			PickupSound,
			GetActorLocation()
		);
	}

	if (PickupEffect)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(
			this,
			PickupEffect,
			GetActorLocation(),
			GetActorRotation(),
			FVector(2.f)
		);
	}
}

