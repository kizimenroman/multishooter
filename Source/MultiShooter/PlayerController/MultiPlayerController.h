// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MultiPlayerController.generated.h"

class AMultiHUD;
class UCharacterOverlay;
class AMultiGameMode;

UCLASS()
class MULTISHOOTER_API AMultiPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	////////////////////////////////////////
	/*
	* Defaults overrides
	*/
	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaTime) override;
	virtual float GetServerTime(); // Synced with server world clock
	virtual void ReceivedPlayer() override; // Sync with server clock ASAP
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;
	////////////////////////////////////////
	/*
	* HUD setups
	*/
	void SetHUDDefeats(int32 Defeats);
	void SetHUDWeaponAmmo(int32 Ammo);
	void SetHUDCarriedAmmo(int32 Ammo);
	void SetHUDHealth(float CurrentHealth, float MaxHealth);
	void SetHUDShield(float CurrentShield, float MaxShield);
	void SetHUDScore(float Score);
	void SetHUDMatchCountdown(float CountdownTime);
	void SetHUDAnnouncementCountdown(float CountdownTime);
	void SetHUDGrenades(int32 Grenades);
	////////////////////////////////////////
	/*
	* Match settings
	*/
	void OnMatchStateSet(FName State);
	void HandleMatchHasStarted();
	void HandleCooldown();

protected:
	virtual void BeginPlay() override;
	void SetHUDTime();
	////////////////////////////////////////
	/*
	* Sync time between client and server
	*/
	UPROPERTY(EditAnywhere, Category = "Network Settings")
	float TimeSyncFreq = 5.f;

	float ClientServerDelta = 0.f; // difference between client and server time
	float TimeSyncRunningTime = 0.f;

	UFUNCTION(Server, Reliable)
	void ServerRequestServerTime(float TimeOfCilentRequest);

	// Reports the current server time to the client in response to ServerRequestServerTime
	UFUNCTION(Client, Reliable)
	void ClientReportServerTime(float TimeClientRequest, float TimeServerReceivedClientRequest);

	UFUNCTION(Server, Reliable)
	void ServerCheckMatchState();

	UFUNCTION(Client, Reliable)
	void ClientJoinMidGame(FName StateOfMatch, float Warmup, float Match, float StartingTime, float CoolTime);

	void HighPingWarning();
	void StopHighPingWarning();

	void CheckTimeSync(float DeltaTime);
	void PollInit();
	void CheckPing(float DeltaTime);

private:
	UPROPERTY()
	AMultiGameMode* MultiGameMode;
	////////////////////////////////////////
	/*
	* HUD setups
	*/
	UPROPERTY()
	AMultiHUD* MultiHUD;

	UPROPERTY()
	UCharacterOverlay* CharacterOverlay;

	bool InitDefeats = false;
	bool InitGrenades = false;
	bool InitScore = false;
	bool InitHealth = false;
	bool InitShield = false;
	bool InitWeaponAmmo = false;
	bool InitCarriedAmmo = false;
	int32 HUDDefeats;
	int32 HUDGrenades;
	float HUDCarriedAmmo;
	float HUDWeaponAmmo;
	float HUDScore;
	float HUDHealth;
	float HUDMaxHealth;
	float HUDShield;
	float HUDMaxShield;
	////////////////////////////////////////
	/*
	* Match settings
	*/
	UPROPERTY(ReplicatedUsing = OnRep_MatchState)
	FName MatchState;

	UFUNCTION()
	void OnRep_MatchState();

	uint32 CountdownInt = 0;
	float MatchTime = 0.f;
	float WarmupTime = 0.f;
	float LevelStartingTime = 0.f;
	float CooldownTime = 0.f;
	////////////////////////////////////////
	/*
	* Net settings
	*/
	float HighPingRunningTime = 0.f;

	UPROPERTY(EditAnywhere)
	float HighPingMaxDuration = 3.f;

	float PingAnimationRunningTime = 0.f;

	UPROPERTY(EditAnywhere)
	float CheckPingFreq = 15.f;

	UPROPERTY(EditAnywhere)
	float HighPingThreshold = 50.f;
};
