// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiPlayerController.h"
#include "MultiShooter/HUD/MultiHUD.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "Net/UnrealNetwork.h"
#include "MultiShooter/Character/MultiCharacter.h"
#include "MultiShooter/GameMode/MultiGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "MultiShooter/HUD/CharacterOverlay.h"
#include "MultiShooter/GameState/MultiGameState.h"
#include "MultiShooter/PlayerState/MultiPlayerState.h"
#include "MultiShooter/MultiComponents/CombatComponent.h"
#include "MultiShooter/HUD/AnnouncementWidget.h"
#include "Components/Image.h"

void AMultiPlayerController::BeginPlay()
{
	Super::BeginPlay();

	MultiHUD = Cast<AMultiHUD>(GetHUD());
	ServerCheckMatchState();
}

void AMultiPlayerController::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMultiPlayerController, MatchState);
}

void AMultiPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AMultiCharacter* MultiChar = Cast<AMultiCharacter>(InPawn);
	if (MultiChar)
	{
		SetHUDHealth(MultiChar->GetCurrentHealth(), MultiChar->GetMaxHealth());
	}
}

void AMultiPlayerController::ReceivedPlayer()
{
	Super::ReceivedPlayer();

	if (IsLocalController())
	{
		ServerRequestServerTime(GetWorld()->GetTimeSeconds());
	}
}

void AMultiPlayerController::CheckPing(float DeltaTime)
{
	HighPingRunningTime += DeltaTime;

	if (HighPingRunningTime >= CheckPingFreq)
	{
		PlayerState = PlayerState == nullptr ? GetPlayerState<APlayerState>() : PlayerState;
		if (PlayerState)
		{
			if (PlayerState->GetPing() * 4 > HighPingThreshold) // GetPing is compressed; It's actually "RealPing / 4"
			{
				HighPingWarning();
				PingAnimationRunningTime = 0.f;
			}
		}
		HighPingRunningTime = 0.f;
	}

	bool bCheckHighAnimationPlaying = MultiHUD && 
		MultiHUD->CharacterOverlay && 
		MultiHUD->CharacterOverlay->HighPingAnimation &&
		MultiHUD->CharacterOverlay->IsAnimationPlaying(MultiHUD->CharacterOverlay->HighPingAnimation);

	if (bCheckHighAnimationPlaying)
	{
		PingAnimationRunningTime += DeltaTime;
		if (PingAnimationRunningTime > HighPingMaxDuration)
		{
			StopHighPingWarning();
		}
	}
}

void AMultiPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetHUDTime();
	CheckTimeSync(DeltaTime);
	PollInit();
	CheckPing(DeltaTime);
}

void AMultiPlayerController::CheckTimeSync(float DeltaTime)
{
	TimeSyncRunningTime += DeltaTime;

	if (IsLocalController() && TimeSyncRunningTime > TimeSyncFreq)
	{
		ServerRequestServerTime(GetWorld()->GetTimeSeconds());
		TimeSyncRunningTime = 0.f;
	}
}

void AMultiPlayerController::HighPingWarning()
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;

	bool bHUDValid = MultiHUD && 
					 MultiHUD->CharacterOverlay && 
					 MultiHUD->CharacterOverlay->HighPingImage && 
					 MultiHUD->CharacterOverlay->HighPingAnimation;

	if (bHUDValid)
	{
		MultiHUD->CharacterOverlay->HighPingImage->SetOpacity(1.f);
		MultiHUD->CharacterOverlay->PlayAnimation(
			MultiHUD->CharacterOverlay->HighPingAnimation,
			0.f,
			5
		);
	}
}

void AMultiPlayerController::StopHighPingWarning()
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;

	bool bHUDValid = MultiHUD && 
					 MultiHUD->CharacterOverlay && 
					 MultiHUD->CharacterOverlay->HighPingImage && 
					 MultiHUD->CharacterOverlay->HighPingAnimation;

	if (bHUDValid)
	{
		MultiHUD->CharacterOverlay->HighPingImage->SetOpacity(0.f);
		if (MultiHUD->CharacterOverlay->IsAnimationPlaying(MultiHUD->CharacterOverlay->HighPingAnimation))
		{
			MultiHUD->CharacterOverlay->StopAnimation( MultiHUD->CharacterOverlay->HighPingAnimation);
		}
	}
}

void AMultiPlayerController::SetHUDTime()
{
	float TimeLeft = 0.f;
	if(MatchState == MatchState::WaitingToStart) TimeLeft = WarmupTime - GetServerTime() + LevelStartingTime;
	else if(MatchState == MatchState::InProgress) TimeLeft = WarmupTime + MatchTime - GetServerTime() + LevelStartingTime;
	else if(MatchState == MatchState::Cooldown) TimeLeft = CooldownTime + WarmupTime + MatchTime - GetServerTime() + LevelStartingTime;
	uint32 SecondsLeft = FMath::CeilToInt(TimeLeft);

	if (HasAuthority())
	{
		MultiGameMode = MultiGameMode == nullptr ? Cast<AMultiGameMode>(UGameplayStatics::GetGameMode(this)) : MultiGameMode;
		if (MultiGameMode)
		{
			SecondsLeft = FMath::CeilToInt(MultiGameMode->GetCountdownTime() + LevelStartingTime);
		}
	}

	if (CountdownInt != SecondsLeft)
	{
		if (MatchState == MatchState::WaitingToStart || MatchState == MatchState::Cooldown)
		{
			SetHUDAnnouncementCountdown(TimeLeft);
		}
		if (MatchState == MatchState::InProgress)
		{
			SetHUDMatchCountdown(TimeLeft);
		}
	}
	CountdownInt = SecondsLeft;
}

void AMultiPlayerController::SetHUDHealth(float CurrentHealth, float MaxHealth)
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;

	bool bHUDValid = MultiHUD && 
					 MultiHUD->CharacterOverlay && 
					 MultiHUD->CharacterOverlay->HealthBar && 
					 MultiHUD->CharacterOverlay->HealthText;

	if (bHUDValid)
	{
		const float HealthPercent = CurrentHealth / MaxHealth;
		MultiHUD->CharacterOverlay->HealthBar->SetPercent(HealthPercent);
		FText SetHealthText = FText::FromString(FString::Printf(TEXT("%d/%d"), FMath::CeilToInt(CurrentHealth), FMath::CeilToInt(MaxHealth)));
		MultiHUD->CharacterOverlay->HealthText->SetText(SetHealthText);
	}
	else
	{
		InitHealth = true;
		HUDHealth = CurrentHealth;
		HUDMaxHealth = MaxHealth;
	}
}

void AMultiPlayerController::SetHUDShield(float CurrentShield, float MaxShield)
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;

	bool bHUDValid = MultiHUD && 
					 MultiHUD->CharacterOverlay && 
					 MultiHUD->CharacterOverlay->ShieldBar && 
					 MultiHUD->CharacterOverlay->ShieldText;

	if (bHUDValid)
	{
		const float ShieldPercent = CurrentShield / MaxShield;
		MultiHUD->CharacterOverlay->ShieldBar->SetPercent(ShieldPercent);
		FText SetShieldText = FText::FromString(FString::Printf(TEXT("%d/%d"), FMath::CeilToInt(CurrentShield), FMath::CeilToInt(MaxShield)));
		MultiHUD->CharacterOverlay->ShieldText->SetText(SetShieldText);
	}
	else
	{
		InitShield = true;
		HUDShield = CurrentShield;
		HUDMaxShield = MaxShield;
	}
}

void AMultiPlayerController::SetHUDScore(float Score)
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;

	bool bHUDValid = MultiHUD && 
				 	 MultiHUD->CharacterOverlay && 
					 MultiHUD->CharacterOverlay->ScoreAmount;
	if (bHUDValid)
	{
		FText TextToSet = FText::FromString(FString::Printf(TEXT("%d"), FMath::FloorToInt(Score)));
		MultiHUD->CharacterOverlay->ScoreAmount->SetText(TextToSet);
	}
	else
	{
		InitScore = true;
		HUDScore = Score;
	}
}

void AMultiPlayerController::SetHUDDefeats(int32 Defeats)
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;

	bool bHUDValid = MultiHUD && 
					 MultiHUD->CharacterOverlay && 
					 MultiHUD->CharacterOverlay->DefeatsAmount;
	if (bHUDValid)
	{
		FText TextToSet = FText::FromString(FString::Printf(TEXT("%d"), Defeats));
		MultiHUD->CharacterOverlay->DefeatsAmount->SetText(TextToSet);
	}
	else
	{
		InitDefeats = true;
		HUDDefeats = Defeats;
	}
}

void AMultiPlayerController::SetHUDWeaponAmmo(int32 Ammo)
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;

	bool bHUDValid = MultiHUD && 
					 MultiHUD->CharacterOverlay && 
					 MultiHUD->CharacterOverlay->WeaponAmmoAmount;
	if (bHUDValid)
	{
		FText TextToSet = FText::FromString(FString::Printf(TEXT("%d"), Ammo));
		MultiHUD->CharacterOverlay->WeaponAmmoAmount->SetText(TextToSet);
	}
	else
	{
		InitWeaponAmmo = true;
		HUDWeaponAmmo = Ammo;
	}
}

void AMultiPlayerController::SetHUDCarriedAmmo(int32 Ammo)
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;

	bool bHUDValid = MultiHUD && 
					 MultiHUD->CharacterOverlay && 
					 MultiHUD->CharacterOverlay->CarriedAmmoAmount;
	if (bHUDValid)
	{
		FText TextToSet = FText::FromString(FString::Printf(TEXT("%d"), Ammo));
		MultiHUD->CharacterOverlay->CarriedAmmoAmount->SetText(TextToSet);
	}
	else
	{
		InitCarriedAmmo = true;
		HUDCarriedAmmo = Ammo;
	}
}

void AMultiPlayerController::SetHUDGrenades(int32 Grenades)
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;

	bool bHUDValid = MultiHUD && 
					 MultiHUD->CharacterOverlay && 
					 MultiHUD->CharacterOverlay->GrenadesText;
	if (bHUDValid)
	{
		FText TextToSet = FText::FromString(FString::Printf(TEXT("%d"), Grenades));
		MultiHUD->CharacterOverlay->GrenadesText->SetText(TextToSet);
	}
	else
	{
		InitGrenades = true;
		HUDGrenades = Grenades;
	}
}

void AMultiPlayerController::SetHUDMatchCountdown(float CountdownTime)
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;
		
		bool bHUDValid = MultiHUD && 
						 MultiHUD->CharacterOverlay && 
						 MultiHUD->CharacterOverlay->MatchCountdownText;
		if (bHUDValid)
		{
			if (CountdownTime < 0.f)
			{
				MultiHUD->CharacterOverlay->MatchCountdownText->SetText(FText());
				return;
			}
			int32 Minutes = FMath::FloorToInt(CountdownTime / 60.f);
			int32 Seconds = CountdownTime - Minutes * 60;

			FText TextToSet = FText::FromString(FString::Printf(TEXT("%02d : %02d"), Minutes, Seconds));
			MultiHUD->CharacterOverlay->MatchCountdownText->SetText(TextToSet);
		}		
}

void AMultiPlayerController::SetHUDAnnouncementCountdown(float CountdownTime)
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;
	bool bHUDValid = MultiHUD    && 
		MultiHUD->AnnounceWidget &&
		MultiHUD->AnnounceWidget->WarmupTime &&
		MultiHUD->AnnounceWidget->InfoText;

	if (bHUDValid)
	{
		if (CountdownTime < 0.f)
		{
			MultiHUD->AnnounceWidget->WarmupTime->SetText(FText());
			return;
		}

		int32 Seconds = FMath::FloorToInt(CountdownTime);

		FText TextToSet = FText::FromString(FString::Printf(TEXT("%02d"), Seconds));
		MultiHUD->AnnounceWidget->WarmupTime->SetText(TextToSet);

		MultiHUD->AnnounceWidget->InfoText->SetText(FText::FromString((TEXT("Please stand by"))));
	}
}

void AMultiPlayerController::ServerRequestServerTime_Implementation(float TimeOfCilentRequest)
{
	float ServerTimeOfReceipt = GetWorld()->GetTimeSeconds();
	ClientReportServerTime(TimeOfCilentRequest, ServerTimeOfReceipt);
}

void AMultiPlayerController::ClientReportServerTime_Implementation(float TimeClientRequest, float TimeServerReceivedClientRequest)
{
	float RoundTripTime = GetWorld()->GetTimeSeconds() - TimeClientRequest;
	float CurrentServerTime = TimeServerReceivedClientRequest + (0.5f * RoundTripTime);

	ClientServerDelta = CurrentServerTime - GetWorld()->GetTimeSeconds(); 
}


float AMultiPlayerController::GetServerTime()
{
	if(HasAuthority()) 
	{
		return GetWorld()->GetTimeSeconds();
	}
	else
	{
		return GetWorld()->GetTimeSeconds() + ClientServerDelta;
	}
}

void AMultiPlayerController::OnMatchStateSet(FName State)
{
	MatchState = State;

	if (MatchState == MatchState::InProgress)
	{
		HandleMatchHasStarted();
	}
	else if (MatchState == MatchState::Cooldown)
	{
		HandleCooldown();
	}
}

void AMultiPlayerController::OnRep_MatchState()
{
	if (MatchState == MatchState::InProgress)
	{
		HandleMatchHasStarted();
	}
	else if (MatchState == MatchState::Cooldown)
	{
		HandleCooldown();
	}
}

void AMultiPlayerController::ServerCheckMatchState_Implementation()
{
	MultiGameMode = Cast<AMultiGameMode>(UGameplayStatics::GetGameMode(this));
	if (MultiGameMode)
	{
		WarmupTime = MultiGameMode->WarmupTime;
		MatchTime = MultiGameMode->MatchTime;
		LevelStartingTime = MultiGameMode->LevelStartingTime;
		MatchState = MultiGameMode->GetMatchState();
		CooldownTime = MultiGameMode->CooldownTime;
		ClientJoinMidGame(MatchState, WarmupTime, MatchTime, LevelStartingTime, CooldownTime);

		if (MultiHUD && MatchState == MatchState::WaitingToStart)
		{
			MultiHUD->AddAnouncement();
		}
	}
}

void AMultiPlayerController::ClientJoinMidGame_Implementation(FName StateOfMatch, float Warmup, float Match, float StartingTime, float CoolTime)
{
	WarmupTime = Warmup;
	MatchTime = Match;
	LevelStartingTime = StartingTime;
	MatchState = StateOfMatch;
	CooldownTime = CoolTime;
	OnMatchStateSet(MatchState);
	
	if (MultiHUD && MatchState == MatchState::WaitingToStart)
	{
		MultiHUD->AddAnouncement();
	}
}

void AMultiPlayerController::HandleMatchHasStarted()
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;
	if (MultiHUD)
	{
		if (MultiHUD->CharacterOverlay == nullptr) MultiHUD->AddCharacterOverlay();
		if (MultiHUD->AnnounceWidget)
		{
			MultiHUD->AnnounceWidget->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}

void AMultiPlayerController::HandleCooldown()
{
	MultiHUD = MultiHUD == nullptr ? Cast<AMultiHUD>(GetHUD()) : MultiHUD;

	if (MultiHUD)
	{
		MultiHUD->CharacterOverlay->RemoveFromParent();
		if (MultiHUD->AnnounceWidget && MultiHUD->AnnounceWidget->InfoText)
		{
			MultiHUD->AnnounceWidget->SetVisibility(ESlateVisibility::Visible);

			AMultiGameState* MultiState = Cast<AMultiGameState>(UGameplayStatics::GetGameState(this));
			AMultiPlayerState* MultiPlayerState = GetPlayerState<AMultiPlayerState>();

			if (MultiState && MultiPlayerState)
			{
				TArray<AMultiPlayerState*> TopPlayers = MultiState->TopScoringPlayers;
				FString AnnouncementText;
				if (TopPlayers.Num() == 0)
				{
					AnnouncementText = FString("There is no winner.");
				}
				else if (TopPlayers.Num() == 1 && TopPlayers[0] == MultiPlayerState)
				{
					AnnouncementText = FString("You are winner!");
				}
				else if (TopPlayers.Num() == 1)
				{
					AnnouncementText = FString::Printf(TEXT("Winner: \n%s"), *TopPlayers[0]->GetPlayerName());
				}
				else if (TopPlayers.Num() > 1)
				{
					AnnouncementText = FString("Players tied for the win:\n");
					for (auto TiedPlayer : TopPlayers)
					{
						AnnouncementText.Append(FString::Printf(TEXT("%s\n"), *TiedPlayer->GetPlayerName()));
					}
				}
				MultiHUD->AnnounceWidget->InfoText->SetText(FText::FromString(AnnouncementText));
			}
		}
	}

	AMultiCharacter* MultiChar = Cast<AMultiCharacter>(GetPawn());
	if (MultiChar)
	{
		MultiChar->bDisableGameplay = true;
		MultiChar->FireButtonReleased();
	}
}

void AMultiPlayerController::PollInit()
{
	if (!CharacterOverlay)
	{
		if (MultiHUD && MultiHUD->CharacterOverlay)
		{
			CharacterOverlay = MultiHUD->CharacterOverlay;
			if (CharacterOverlay)
			{
				if (InitHealth) SetHUDHealth(HUDHealth, HUDMaxHealth);
				if (InitScore) SetHUDScore(HUDScore);
				if (InitDefeats) SetHUDDefeats(HUDDefeats);
				if (InitShield) SetHUDShield(HUDShield, HUDMaxShield);
				if (InitWeaponAmmo) SetHUDWeaponAmmo(HUDWeaponAmmo);
				if (InitCarriedAmmo) SetHUDCarriedAmmo(HUDCarriedAmmo);

				AMultiCharacter* MultiCharacter = Cast<AMultiCharacter>(GetPawn());
				if (MultiCharacter && MultiCharacter->GetCombat())
				{
					if (InitGrenades) SetHUDGrenades(MultiCharacter->GetCombat()->GetGrenades());
				}
			}
		}
	}
}